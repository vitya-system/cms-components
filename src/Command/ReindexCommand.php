<?php

/*
 * Copyright 2022 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Command;

use Vitya\Application\Application;
use Vitya\CmsComponent\Entity\EntityInterface;
use Vitya\CmsComponent\IndexManagerInterface;
use Vitya\Component\Command\CommandInterface;
use Vitya\Component\Command\Terminal;

class ReindexCommand implements CommandInterface
{
    private $extensions = [];

    public function __construct(array $extensions)
    {
        $this->extensions = $extensions;
    }

    public function getName(): string
    {
        return 'reindex';
    }

    public function execute(Terminal $io): int
    {
        $io->nl();
        foreach ($this->extensions as $extension) {
            if ($extension instanceof IndexManagerInterface) {
                $io->text('Purge indexes for ' . (string) $extension . '... ');
                $extension->purgeIndex();
                $io->success('Done.');
                $io->nl();
            }
        }
        foreach ($this->extensions as $extension) {
            if ($extension instanceof IndexManagerInterface) {
                $io->nl();
                $io->text('Recreate indexes for ' . (string) $extension . '... ');
                $io->nl();
                foreach ($extension->indexAll() as $element) {
                    if ($element instanceof EntityInterface) {
                        $io->text($element->getMachineName() . ' #' . $element->getId() . ' indexed.' );
                        $io->nl();
                    }
                }
                $io->success('Done.');
                $io->nl();
            }
        }
        return 0;
    }

}
