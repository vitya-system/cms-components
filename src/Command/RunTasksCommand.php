<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Command;

use Psr\Log\LogLevel;
use Vitya\CmsComponent\Task\TaskManagerInterface;
use Vitya\CmsComponent\Task\TaskMessageInterface;
use Vitya\Component\Command\CommandInterface;
use Vitya\Component\Command\Terminal;

class RunTasksCommand implements CommandInterface
{
    private $taskManager = null;

    public function __construct(TaskManagerInterface $task_manager)
    {
        $this->taskManager = $task_manager;
    }

    public function getName(): string
    {
        return 'run-tasks';
    }

    public function execute(Terminal $io): int
    {
        $this->taskManager->init();
        foreach ($this->taskManager->run() as $task_message) {
            if ($task_message instanceof TaskMessageInterface) {
                switch ($task_message->getType()) {
                    case LogLevel::EMERGENCY:
                    case LogLevel::ALERT:
                    case LogLevel::CRITICAL:
                    case LogLevel::ERROR:
                        $io->error($task_message->getMessage());
                        break;
                    case LogLevel::WARNING:
                        $io->info($task_message->getMessage());
                        break;
                    case LogLevel::NOTICE:
                        $io->success($task_message->getMessage());
                        break;
                    case LogLevel::INFO:
                    case LogLevel::DEBUG:
                    default:
                        $io->text($task_message->getMessage());
                }
                $io->nl();
            }
        }
        return 0;
    }

    public function getTaskManager(): TaskManagerInterface
    {
        return $this->taskManager;
    }

}
