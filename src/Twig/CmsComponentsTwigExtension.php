<?php

/*
 * Copyright 2022 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Twig;

use Exception;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Vitya\CmsComponent\Describable\DescribableInterface;
use Vitya\CmsComponent\EntityComponent\EntityComponentInterface;
use Vitya\CmsComponent\Image\ResponsiveImageHelper;
use Vitya\CmsComponent\Image\ResponsiveImageSourceInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class CmsComponentsTwigExtension extends AbstractExtension
{
    private $applicationParameters = [];
    private $dependencyInjector = null;

    public function __construct(
        array $app_params,
        DependencyInjectorInterface $dependency_injector
    ) {
        $this->applicationParameters = $app_params;
        $this->dependencyInjector = $dependency_injector;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('ao', [$this, 'ao']),
            new TwigFunction('describe', [$this, 'describe']),
            new TwigFunction('image_url', [$this, 'imageUrl']),
        ];
    }

    private function getServiceFromHint(string $hint): object
    {
        $service = $this->dependencyInjector->getServiceFromHint($hint);
        if ($service === null) {
            throw new Exception('Could not locate a service for ' . $hint . '.');
        }
        return $service;
    }

    public function ao(string $address)
    {
        $addressable_object_store = $this->getServiceFromHint('Vitya\CmsComponent\AddressableObject\AddressableObjectStoreInterface');
        $obj = $addressable_object_store->getFromAddress($address);
        if (null === $obj) {
            return null;
        }
        return $obj;
    }

    public function describe($thing)
    {
        if (false === $thing instanceof DescribableInterface) {
            return 'not a describable object';
        }
        return $this->printDescription($thing->describe());
    }

    public function printDescription($description, int $level = 0)
    {
        if ($level > 50) {
            return '';
        }
        $s = '';
        if (is_array($description)) {
            $s .= "\n";
            foreach ($description as $k => $v) {
                $s .= str_repeat(' ', $level * 4) . $k . ': ' . $this->printDescription($v, $level + 1);
            }
        } else {
            $s .= (string) $description . "\n";
        }
        return $s;
    }

    public function imageUrl(string $component_address, string $output_format): string
    {
        $router = $this->getServiceFromHint('Vitya\Component\Route\RouterInterface');
        $addressable_object_store = $this->getServiceFromHint('Vitya\CmsComponent\AddressableObject\AddressableObjectStoreInterface');
        $image_factory = $this->getServiceFromHint('Vitya\Component\Image\ImageFactoryInterface');
        $logger_collection = $this->getServiceFromHint('Vitya\Component\Log\LoggerCollectionInterface');
        $logger = $logger_collection->getLogger('app');
        $component = $addressable_object_store->getFromAddress($component_address);
        if (null === $component) {
            $logger->error('Could not find a component at address ' . $component_address . '.');
            return '';
        }
        if (false === $component instanceof ResponsiveImageSourceInterface || false === $component instanceof EntityComponentInterface) {
            $logger->error('Component at address ' . $component_address . ' must implement ResponsiveImageSourceInterface and EntityComponentInterface.');
            return '';
        }
        $entity = $component->getEntity();
        $available_output_formats = $component->getAvailableOutputFormats();
        if (false === isset($available_output_formats[$output_format])) {
            $logger->error('Format "' . $output_format . '" not available for component at address ' . $component_address . '.');
            return '';
        }
        $output_format_description = $available_output_formats[$output_format];
        // Check whether filename extension matches.
        $responsive_image_helper = new ResponsiveImageHelper($component, $image_factory);
        $output_file_format = $responsive_image_helper->getOutputFileFormat($component->mime_type, $output_format_description['mime_type']);
        $filename_extension = $output_file_format['filename_extension'];
        $route_name = $this->applicationParameters['default_image_controller_route_name'];
        $parameters = [
            'validator' => (string) $entity->getLastModificationUts(),
            'output_format' => $output_format,
            'component_address' => $component->getAddress(),
            'filename_extension' => $filename_extension,
        ];
        return (string) $router->createUri($route_name, $parameters);
    }

}
