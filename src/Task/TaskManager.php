<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Task;

use DateTime;
use Generator;
use Psr\Log\LogLevel;
use Throwable;
use Vitya\CmsComponent\Db\CmsDb;
use Vitya\Component\Log\LoggerCollectionInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class TaskManager implements TaskManagerInterface
{
    private $cmsDb = null;
    private $dependencyInjector = null;
    private $loggerCollection = null;
    private $startUts = 0;
    private $recurringTasks = [];
    private $oneShotTasks = [];

    public function __construct(CmsDb $cms_db, DependencyInjectorInterface $dependency_injector, LoggerCollectionInterface $logger_collection)
    {
        $this->cmsDb = $cms_db;
        $this->dependencyInjector = $dependency_injector;
        $this->loggerCollection = $logger_collection;
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getLoggerCollection(): LoggerCollectionInterface
    {
        return $this->loggerCollection;
    }

    public function init(): TaskManagerInterface
    {
        // Manage the reference timestamp for this run.
        $this->startUts = time();
        $last_run_uts = (int) $this->cmsDb->getConnection()->fetchOne(
            'SELECT value FROM ' . $this->cmsDb->getPrefix() . '_task_manager_registry WHERE k = "last_run_uts"'
        );
        if (($this->startUts - $last_run_uts) > 7200) {
            $last_run_uts = $this->startUts - 7200;
        }
        try {
            $this->cmsDb->getConnection()->insert(
                $this->cmsDb->getPrefix() . '_task_manager_registry',
                [
                    'k' => 'last_run_uts',
                    'value' => (string) $this->startUts,
                ]
            );
        } catch (Throwable $t) {
            // The line is already present.
        }
        $this->cmsDb->getConnection()->update(
            $this->cmsDb->getPrefix() . '_task_manager_registry', 
            [
                'value' => (string) $this->startUts,
            ],
            [
                'k' => 'last_run_uts',
            ]
        );
        // Determine which one-minute spans have passed since last run.
        $last_run_dt = new DateTime();
        $last_run_dt->setTimestamp($last_run_uts);
        $last_run_cron_line = $last_run_dt->format('i-H-d-m-w');
        $uts = $this->startUts;
        $dt = new DateTime();
        $dt->setTimestamp($uts);
        $cron_lines_to_process = [];
        while ($uts > $last_run_uts) {
            $cron_line = $dt->format('i-H-d-m-w');
            if ($cron_line != $last_run_cron_line) {
                $cron_lines_to_process[] = $cron_line;
            }
            $uts -= 60;
            $dt->setTimestamp($uts);
        }
        $cron_lines_to_process = array_reverse($cron_lines_to_process);
        // Prepare recurring tasks list.
        $this->recurringTasks = [];
        $recurring_tasks_in_db = $this->cmsDb->getConnection()->fetchAllAssociative(''
            . 'SELECT class_name, k, parameters, minute, hour, dom, month, dow '
            . 'FROM ' . $this->cmsDb->getPrefix() . '_task_manager_recurring_task' . ' '
            . 'ORDER BY class_name, k'
        );
        foreach ($cron_lines_to_process as $cron_line_to_process) {
            foreach ($recurring_tasks_in_db as $recurring_task_in_db) {
                if ($this->cronLineMatches(
                    $cron_line_to_process,
                    $recurring_task_in_db['minute'],
                    $recurring_task_in_db['hour'],
                    $recurring_task_in_db['dom'],
                    $recurring_task_in_db['month'],
                    $recurring_task_in_db['dow'])) 
                {
                    $this->recurringTasks[] = [
                        'class_name' => $recurring_task_in_db['class_name'],
                        'k' => $recurring_task_in_db['k'],
                        'parameters' => json_decode($recurring_task_in_db['parameters'], true),
                    ];
                }
            }
        }
        // Prepare one-shot tasks list.
        $this->oneShotTasks = [];
        $one_shot_tasks_in_db = $this->cmsDb->getConnection()->fetchAllAssociative(''
            . 'SELECT class_name, k, parameters '
            . 'FROM ' . $this->cmsDb->getPrefix() . '_task_manager_one_shot_task' . ' '
            . 'WHERE scheduled_uts <= ' . time() . ' '
            . 'ORDER BY id'
        );
        foreach ($one_shot_tasks_in_db as $one_shot_task_in_db) {
            $this->oneShotTasks[] = [
                'class_name' => $one_shot_task_in_db['class_name'],
                'k' => $one_shot_task_in_db['k'],
                'parameters' => json_decode($one_shot_task_in_db['parameters'], true),
            ];
        }
        return $this;
    }

    public function run(): Generator
    {
        // Recurring tasks.
        while (null !== $next_line = array_shift($this->recurringTasks)) {
            $this->cmsDb->getConnection()->update(
                $this->cmsDb->getPrefix() . '_task_manager_recurring_task',
                [
                    'last_run_uts' => time(),
                ],
                [
                    'class_name' => $next_line['class_name'],
                    'k' => $next_line['k'],
                ]
            );
            yield $this->logLine(new TaskMessage(
                'Starting recurring task ' . $next_line['class_name'] . ', k = ' . $next_line['k'] . '...',
                LogLevel::INFO,
                $next_line['parameters']
            ));
            if (false === class_exists($next_line['class_name'])) {
                yield $this->logLine(new TaskMessage('Class "' . $next_line['class_name'] . '" doesn\'t exist.', LogLevel::CRITICAL));
                continue;
            }
            $implemented_interfaces = class_implements($next_line['class_name']);
            if (false === in_array('Vitya\CmsComponent\Task\TaskInterface', $implemented_interfaces)) {
                yield $this->logLine(new TaskMessage('Class "' . $next_line['class_name'] . '" doesn\'t implement TaskInterface.', LogLevel::CRITICAL));
                continue;
            }
            $task = $this->dependencyInjector->make($next_line['class_name']);
            $task->setParameters($next_line['parameters']);
            foreach ($task->run() as $task_message) {
                if ($task_message instanceof TaskMessageInterface) {
                    yield $this->logLine($task_message);
                } else {
                    yield $this->logLine(new TaskMessage('Task yielded an invalid message (TextMessageInterface expected).', LogLevel::CRITICAL));
                }
            }
        }
        // One-shot tasks, as long as we have enough time left before a new task manager job starts.
        while (null !== $next_line = array_shift($this->oneShotTasks)) {
            $now_uts = time();
            if (($now_uts - $this->startUts) > 50) {
                break;
            }
            $this->cmsDb->getConnection()->delete(
                $this->cmsDb->getPrefix() . '_task_manager_one_shot_task',
                [
                    'class_name' => $next_line['class_name'],
                    'k' => $next_line['k'],
                ]
            );
            yield $this->logLine(new TaskMessage(
                'Starting one-shot task ' . $next_line['class_name'] . ', k = ' . $next_line['k'] . '...',
                LogLevel::INFO,
                $next_line['parameters']
            ));
            if (false === class_exists($next_line['class_name'])) {
                yield $this->logLine(new TaskMessage('Class "' . $next_line['class_name'] . '" doesn\'t exist.', LogLevel::CRITICAL));
                continue;
            }
            $implemented_interfaces = class_implements($next_line['class_name']);
            if (false === in_array('Vitya\CmsComponent\Task\TaskInterface', $implemented_interfaces)) {
                yield $this->logLine(new TaskMessage('Class "' . $next_line['class_name'] . '" doesn\'t implement TaskInterface.', LogLevel::CRITICAL));
                continue;
            }
            $task = $this->dependencyInjector->make($next_line['class_name']);
            $task->setParameters($next_line['parameters']);
            foreach ($task->run() as $task_message) {
                if ($task_message instanceof TaskMessageInterface) {
                    yield $this->logLine($task_message);
                } else {
                    yield $this->logLine(new TaskMessage('Task yielded an invalid message (TextMessageInterface expected).', LogLevel::CRITICAL));
                }
            }
        }
    }

    public function registerRecurringTask(
        string $class_name,
        string $k,
        array $parameters,
        string $minute,
        string $hour,
        string $dom,
        string $month,
        string $dow
    ): TaskManagerInterface
    {
        $parameters_string = json_encode($parameters);
        try {
            $this->cmsDb->getConnection()->insert(
                $this->cmsDb->getPrefix() . '_task_manager_recurring_task',
                [
                    'class_name' => $class_name,
                    'k' => $k,
                    'parameters' => $parameters_string,
                    'minute' => '',
                    'hour' => '',
                    'dom' => '',
                    'month' => '',
                    'dow' => '',
                    'last_run_uts' => 0,
                ]
            );
        } catch (Throwable $t) {
            // The line is already present.
        }
        $this->cmsDb->getConnection()->update(
            $this->cmsDb->getPrefix() . '_task_manager_recurring_task',
            [
                'parameters' => $parameters_string,
                'minute' => $minute,
                'hour' => $hour,
                'dom' => $dom,
                'month' => $month,
                'dow' => $dow,
            ],
            [
                'class_name' => $class_name,
                'k' => $k,
            ]
        );
        return $this;
    }

    public function deleteRecurringTask(string $class_name, string $k): TaskManagerInterface
    {
        $this->cmsDb->getConnection()->delete(
            $this->cmsDb->getPrefix() . '_task_manager_recurring_task',
            [
                'class_name' => $class_name,
                'k' => $k,
            ]
        );
        return $this;
    }

    public function deleteAllRecurringTasks(string $class_name): TaskManagerInterface
    {
        $this->cmsDb->getConnection()->delete(
            $this->cmsDb->getPrefix() . '_task_manager_recurring_task',
            [
                'class_name' => $class_name,
            ]
        );
        return $this;
    }

    public function registerOneShotTask(
        string $class_name,
        string $k,
        array $parameters,
        int $scheduled_uts = 0
    ): TaskManagerInterface
    {
        $parameters_string = json_encode($parameters);
        try {
            $this->cmsDb->getConnection()->insert(
                $this->cmsDb->getPrefix() . '_task_manager_one_shot_task',
                [
                    'class_name' => $class_name,
                    'k' => $k,
                    'parameters' => $parameters_string,
                    'scheduled_uts' => $scheduled_uts,
                ]
            );
        } catch (Throwable $t) {
            // The line is already present.
            try {
                $this->cmsDb->getConnection()->update(
                    $this->cmsDb->getPrefix() . '_task_manager_one_shot_task',
                    [
                        'parameters' => $parameters_string,
                        'scheduled_uts' => $scheduled_uts,
                    ],
                    [
                        'class_name' => $class_name,
                        'k' => $k,
                    ]
                );
            } catch (Throwable $t) {
                throw $t;
            }
        }
        return $this;
    }

    public function deleteOneShotTask(string $class_name, string $k): TaskManagerInterface
    {
        $this->cmsDb->getConnection()->delete(
            $this->cmsDb->getPrefix() . '_task_manager_one_shot_task',
            [
                'class_name' => $class_name,
                'k' => $k,
            ]
        );
        return $this;
    }

    public function deleteAllOneShotTasks(string $class_name): TaskManagerInterface
    {
        $this->cmsDb->getConnection()->delete(
            $this->cmsDb->getPrefix() . '_task_manager_one_shot_task',
            [
                'class_name' => $class_name,
            ]
        );
        return $this;
    }

    public function cronLineMatches(string $cron_line, string $minute, string $hour, string $dom, string $month, string $dow): bool
    {
        list($input_minute, $input_hour, $input_dom, $input_month, $input_dow) = explode('-', $cron_line);
        if (false === $this->matchesExpression($input_minute, $minute)) {
            return false;
        }
        if (false === $this->matchesExpression($input_hour, $hour)) {
            return false;
        }
        if (false === $this->matchesExpression($input_dom, $dom)) {
            return false;
        }
        if (false === $this->matchesExpression($input_month, $month)) {
            return false;
        }
        if (false === $this->matchesExpression($input_dow, $dow)) {
            return false;
        }
        return true;
    }

    public function matchesExpression(string $input, string $expression): bool
    {
        // Asterisk.
        if ('*' === $expression) {
            return true;
        }
        $int_input = (int) $input;
        // Exact match.
        if (preg_match('@^[0-9]+$@', $expression)) {
            if ($int_input === (int) $expression) {
                return true;
            }
        }
        // Range, eg.: 3-5.
        if (preg_match('@^[0-9]+\-[0-9]+$@', $expression)) {
            list($start, $end) = explode('-', $expression);
            if ($int_input >= (int) $start && $int_input <= (int) $end) {
                return true;
            }
        }
        // Divisble by a certain number, eg.: */5.
        if (preg_match('@^\*/[0-9]+$@', $expression)) {
            list($star, $diviser) = explode('/', $expression);
            if (0 === ($int_input % (int) $diviser)) {
                return true;
            }
        }
        return false;
    }

    public function logLine(TaskMessageInterface $task_message): TaskMessageInterface
    {
        $logger = $this->loggerCollection->getLogger('task_manager');
        $logger->log($task_message->getType(), $task_message->getMessage(), $task_message->getAdditionalInfo());
        return $task_message;
    }

}
