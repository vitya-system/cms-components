<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Task;

use Generator;
use Psr\Log\LogLevel;
use Throwable;
use Vitya\CmsComponent\Entity\EntityFactoryInterface;
use Vitya\CmsComponent\Entity\EntityList;
use Vitya\CmsComponent\EntityListModifier\EntityFilter;

class DeleteOldPartiallyCreatedEntitiesTask implements TaskInterface
{
    private $entityFactory = null;

    public function __construct(EntityFactoryInterface $entity_factory)
    {
        $this->entityFactory = $entity_factory;
    }

    public function getEntityFactoryInterface(): EntityFactoryInterface
    {
        return $this->entityFactory;
    }

    public function setParameters(array $parameters): TaskInterface
    {
        return $this;
    }
    
    public function run(): Generator
    {
        yield new TaskMessage('Delete old partially created entities...', LogLevel::INFO);
        foreach ($this->entityFactory->getAvailableEntityClassNames() as $class_name) {
            $entity_model = $this->entityFactory->make($class_name);
            $entity_list = new EntityList($entity_model, [
                'exclude_fully_created_entities' => true,
                'exclude_non_fully_created_entities' => false,
            ]);
            $entity_list->addModifier(new EntityFilter('creation_uts', '<', time() - 24 * 3600));
            $nb_deleted = 0;
            foreach ($entity_list->getIds() as $entity_id) {
                try {
                    $entity = $this->entityFactory->load($class_name, $entity_id);
                    $entity->delete();
                    $nb_deleted++;
                } catch (Throwable $t) {
                    yield new TaskMessage(
                        'Entity ' . $entity_model->getMachineName() . ' #' . $entity_id . ' couldn\'t be deleted (' . $t->getMessage() . ').', 
                        LogLevel::WARNING
                    );
                }
            }
            if ($nb_deleted > 0) {
                yield new TaskMessage($nb_deleted . ' partially created ' . $entity_model->getMachineName() . ' entity(ies) deleted.', LogLevel::NOTICE);
            }
        }
        yield new TaskMessage('Done.', LogLevel::INFO);
    }

}
