<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Task;

use Psr\Log\LogLevel;

class TaskMessage implements TaskMessageInterface
{
    private $message = '';
    private $type = '';
    private $additionalInfo = [];

    public function __construct(string $message, string $type = LogLevel::INFO, array $additional_info = [])
    {
        $this->message = $message;
        $this->type = $type;
        $this->additionalInfo = $additional_info;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getAdditionalInfo(): array
    {
        return $this->additionalInfo;
    }

}
