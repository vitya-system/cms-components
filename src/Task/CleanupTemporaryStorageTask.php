<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Task;

use Generator;
use Psr\Log\LogLevel;
use Throwable;
use Vitya\Component\TemporaryStorage\TemporaryStorageInterface;

class CleanupTemporaryStorageTask implements TaskInterface
{
    private $temporaryStorage = null;

    public function __construct(TemporaryStorageInterface $temporary_storage)
    {
        $this->temporaryStorage = $temporary_storage;
    }

    public function getTemporaryStorage(): TemporaryStorageInterface
    {
        return $this->temporaryStorage;
    }

    public function setParameters(array $parameters): TaskInterface
    {
        return $this;
    }
    
    public function run(): Generator
    {
        yield new TaskMessage('Cleanup temporary storage...', LogLevel::INFO);
        $nb_deleted = 0;
        foreach ($this->temporaryStorage->purgeOldFiles() as $filename) {
            $nb_deleted++;
            if (0 === $nb_deleted % 10) {
                yield new TaskMessage($nb_deleted . ' files deleted...', LogLevel::NOTICE);
            }
        }
        yield new TaskMessage('Done, ' . $nb_deleted . ' file(s) deleted.', LogLevel::INFO);
    }

}
