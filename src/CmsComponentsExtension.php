<?php

/*
 * Copyright 2020, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent;

use Doctrine\DBAL\Schema\Schema;
use Generator;
use Vitya\Application\Application;
use Vitya\Application\ApplicationExtensionInterface;
use Vitya\Application\ServiceProvider\DbServiceProvider;
use Vitya\Application\ServiceProvider\L10nServiceProvider;
use Vitya\CmsComponent\ServiceProvider\AddressableObjectStoreServiceProvider;
use Vitya\CmsComponent\ServiceProvider\CliFrontendServiceExtender;
use Vitya\CmsComponent\ServiceProvider\CmsDbServiceProvider;
use Vitya\CmsComponent\ServiceProvider\CmsUserProviderServiceProvider;
use Vitya\CmsComponent\ServiceProvider\EntityComponentServiceProvider;
use Vitya\CmsComponent\ServiceProvider\EntityServiceProvider;
use Vitya\CmsComponent\ServiceProvider\TaskManagerServiceProvider;

class CmsComponentsExtension implements ApplicationExtensionInterface, CmsDbSchemaModifierInterface, IndexManagerInterface
{
    private $app = null;

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->app->mergeConfig([
            'default_image_controller_route_name' => 'image',
            'use_default_image_controller' => true,
            'default_maintenance_tasks_time_minute' => '14',
            'default_maintenance_tasks_time_hour' => '3',
            'default_maintenance_tasks_time_dow' => '*',
            'default_maintenance_tasks_time_month' => '*',
            'default_maintenance_tasks_time_dom' => '*',
            'cms_db__prefix' => 'vitya',
            'entity__models' => [],
            'cms_user_provider__supported_class' => '',
            'cms_user_provider__user_groups' => [
                1 => 'webmasters',
                2 => 'editors',
                3 => 'collaborators',
            ],
            'cms_user_provider__sysadmin_user_group' => 1,
        ]);
    }

    public function __toString(): string
    {
        return 'Vitya CMS Application Extension';
    }

    public function parametrize(): void
    {
        $this->app['default_image_controller_route_name'] = (string) $this->app['config']['default_image_controller_route_name'];
        $this->app['use_default_image_controller'] = (bool) $this->app['config']['use_default_image_controller'];
        $this->app['default_maintenance_tasks_time_minute'] = (string) $this->app['config']['default_maintenance_tasks_time_minute'];
        $this->app['default_maintenance_tasks_time_hour'] = (string) $this->app['config']['default_maintenance_tasks_time_hour'];
        $this->app['default_maintenance_tasks_time_dow'] = (string) $this->app['config']['default_maintenance_tasks_time_dow'];
        $this->app['default_maintenance_tasks_time_month'] = (string) $this->app['config']['default_maintenance_tasks_time_month'];
        $this->app['default_maintenance_tasks_time_dom'] = (string) $this->app['config']['default_maintenance_tasks_time_dom'];
        // CmsDb parameters.
        $this->app['cms_db__prefix'] = (string) $this->app['config']['cms_db__prefix'];
        // Db parameters.
        if (false === isset($this->app['db']['cms'])) {
            $this->app['db'] = [
                'cms' => [
                    'url' => 'pdo-sqlite:///' . $this->app['data_dir'] . '/cms.sqlite3',
                ]
            ] + $this->app['db'];
        }
        // Log service parameters.
        if (false === isset($this->app['log']['task_manager'])) {
            $this->app['log'] = [
                'task_manager' => [
                    'type' => 'basic',
                    'options' => [
                        'path' => '{log_dir}/task_manager.log',
                        'rotate' => true,
                    ],
                ],
            ] + $this->app['log'];
        }
        // Entity service parameters.
        $this->app['entity__entity_models'] = [];
        $sanitized_entity_models = [];
        $entity_models = $this->app['config']['entity__entity_models'];
        if (is_array($entity_models)) {
            foreach ($entity_models as $entity_model) {
                $sanitized_entity_models[] = (string) $entity_model;
            }
        }
        $this->app['entity__entity_models'] = $sanitized_entity_models;
        // User provider service parameters.
        $this->app['cms_user_provider__supported_class'] = (string) $this->app['config']['cms_user_provider__supported_class'];
        $this->app['cms_user_provider__user_groups'] = [];
        $sanitized_user_groups = [];
        $user_groups = $this->app['config']['cms_user_provider__user_groups'];
        if (is_array($user_groups)) {
            foreach ($user_groups as $user_group_identifier => $user_group_name) {
                $sanitized_user_groups[(int) $user_group_identifier] = $user_group_name;
            }
        }
        $this->app['cms_user_provider__user_groups'] = $sanitized_user_groups;
        $this->app['cms_user_provider__sysadmin_user_group'] = (int) $this->app['config']['cms_user_provider__sysadmin_user_group'];
        // Register services.
        $this->app->register(new AddressableObjectStoreServiceProvider());
        $this->app->register(new DbServiceProvider());
        $this->app->register(new CmsDbServiceProvider());
        $this->app->register(new EntityComponentServiceProvider());
        $this->app->register(new EntityServiceProvider());
        $this->app->register(new L10nServiceProvider());
        $this->app->register(new CmsUserProviderServiceProvider());
        $this->app->register(new TaskManagerServiceProvider());
        $this->app->extend(new CliFrontendServiceExtender());
        // Routes.
        if ($this->app['use_default_image_controller']) {
            $this->app->mergeRoutes([
                $this->app['default_image_controller_route_name'] => [
                    'path' => '/image/{validator}/{output_format}{component_address}.{filename_extension}',
                    'callable' => 'Vitya\\CmsComponent\\Controller\\ImageController::image',
                    'methods' => ['GET'],
                    'regexes' => [
                        'component_address' => '[a-z0-9\\-/]+',
                        'filename_extension' => '[a-z0-9]+',
                    ],
                ],
            ]);
        }
    }

    public function init(): void
    {
    }

    public function install(): void
    {
        $schema_manager = $this->app->get('cms_db')->getConnection()->createSchemaManager();
        $previous_schema = $schema_manager->introspectSchema();
        $updated_schema = new Schema();
        foreach ($this->app->getExtensions() as $extension) {
            if ($extension instanceof CmsDbSchemaModifierInterface) {
                $updated_schema = $extension->modifySchema($updated_schema);
            }
        }
        $comparator = $schema_manager->createComparator();
        $schema_diff = $comparator->compareSchemas($previous_schema, $updated_schema);
        $schema_manager->alterSchema($schema_diff);
        foreach ($this->app->get('entity')->getAvailableEntityClassNames() as $entity_class_name) {
            $entity_model = $this->app->get('entity')->make($entity_class_name);
            $entity_model->install();
        }
        $this->app->get('task_manager')->registerRecurringTask(
            'Vitya\CmsComponent\Task\DeleteOldPartiallyCreatedEntitiesTask', 
            'delete_partially_created_entities',
            [],
            $this->app['default_maintenance_tasks_time_minute'],
            $this->app['default_maintenance_tasks_time_hour'],
            $this->app['default_maintenance_tasks_time_dow'],
            $this->app['default_maintenance_tasks_time_month'],
            $this->app['default_maintenance_tasks_time_dom']
        );
        $this->app->get('task_manager')->registerRecurringTask(
            'Vitya\CmsComponent\Task\CleanupTemporaryStorageTask', 
            'cleanup_temporary_storage',
            [],
            $this->app['default_maintenance_tasks_time_minute'],
            $this->app['default_maintenance_tasks_time_hour'],
            $this->app['default_maintenance_tasks_time_dow'],
            $this->app['default_maintenance_tasks_time_month'],
            $this->app['default_maintenance_tasks_time_dom']
        );
    }

    public function modifySchema(Schema $schema): Schema
    {
        // Entity tables.
        foreach ($this->app->get('entity')->getAvailableEntityClassNames() as $entity_class_name) {
            $entity_model = $this->app->get('entity')->make($entity_class_name);
            $schema = $entity_model->modifySchema($schema);
        }
        // Nonces.
        $table = $schema->createTable($this->app->get('cms_db')->getPrefix() . '_nonce');
        $table->addColumn('token', 'string', ['length' => 40]);
        $table->addColumn('action', 'string', []);
        $table->addColumn('expiration_uts', 'integer', []);
        $table->addUniqueIndex(['token']);
        $table->addIndex(['expiration_uts']);
        // Collection info.
        $table = $schema->createTable($this->app->get('cms_db')->getPrefix() . '_collection_info');
        $table->addColumn('class_name', 'string', ['length' => 120]);
        $table->addColumn('last_modification_uts', 'integer');
        $table->addColumn('content', 'text');
        $table->addUniqueIndex(['class_name']);
        // Task manager registry.
        $table = $schema->createTable($this->app->get('cms_db')->getPrefix() . '_task_manager_registry');
        $table->addColumn('k', 'string', ['length' => 120]);
        $table->addColumn('value', 'text');
        $table->addUniqueIndex(['k']);
        // Task manager: recurring tasks.
        $table = $schema->createTable($this->app->get('cms_db')->getPrefix() . '_task_manager_recurring_task');
        $table->addColumn('class_name', 'string', ['length' => 120]);
        $table->addColumn('k', 'string', ['length' => 120]);
        $table->addColumn('parameters', 'text');
        $table->addColumn('minute', 'string', ['length' => 10]);
        $table->addColumn('hour', 'string', ['length' => 10]);
        $table->addColumn('dom', 'string', ['length' => 10]);
        $table->addColumn('month', 'string', ['length' => 10]);
        $table->addColumn('dow', 'string', ['length' => 10]);
        $table->addColumn('last_run_uts', 'integer');
        $table->addUniqueIndex(['class_name', 'k']);
        // Task manager: one-shot tasks.
        $table = $schema->createTable($this->app->get('cms_db')->getPrefix() . '_task_manager_one_shot_task');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('class_name', 'string', ['length' => 120]);
        $table->addColumn('k', 'string', ['length' => 120]);
        $table->addColumn('parameters', 'text');
        $table->addColumn('scheduled_uts', 'integer');
        $table->setPrimaryKey(['id']);
        $table->addUniqueIndex(['class_name', 'k']);
        return $schema;
    }

    public function purgeIndex(): void
    {
        foreach ($this->app->get('entity')->getAvailableEntityClassNames() as $entity_class_name) {
            $entity_model = $this->app->get('entity')->make($entity_class_name);
            $entity_model->purgeIndex();
        }
    }

    public function indexAll(): Generator
    {
        foreach ($this->app->get('entity')->getAvailableEntityClassNames() as $entity_class_name) {
            $entity_model = $this->app->get('entity')->make($entity_class_name);
            if ($entity_model instanceof IndexableInterface) {
                $ids = $entity_model->getIds();
                foreach ($ids as $id) {
                    $entity = $entity_model->load($id);
                    $entity->index();
                    yield $entity;
                }
            }
        }
    }

}
