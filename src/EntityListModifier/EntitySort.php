<?php

/*
 * Copyright 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityListModifier;

use Doctrine\DBAL\Query\QueryBuilder;
use Exception;

class EntitySort extends AbstractEntityListModifier
{
    private $property = '';
    private $direction = 'ASC';

    public function __construct(string $property, string $direction)
    {
        $this->property = trim($property);
        if ('DESC' === strtoupper(trim($direction))) {
            $this->direction = 'DESC';
        }
    }

    public function updateQueryBuilder(QueryBuilder $query_builder): AbstractEntityListModifier
    {
        if ('id' === $this->property) {
            $query_builder->addOrderBy('entity.id', $this->direction);
        } elseif ('last_modification_time' === $this->property) {
            $query_builder->addOrderBy('entity.last_modification_uts', $this->direction);
        } else {
            throw new Exception('Unknown sort property: "' . $this->property . '".');
        }
        return $this;
    }

    public function getProperty(): string
    {
        return $this->property;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }

}
