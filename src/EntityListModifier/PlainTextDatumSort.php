<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityListModifier;

use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use Vitya\CmsComponent\Datum\PlainTextDatum;

class PlainTextDatumSort extends AbstractEntityListModifier
{
    private $datumRelativeAddress = '';
    private $direction = 'ASC';
    private $indexTableAlias = '';

    public function __construct(string $datum_relative_address, string $direction)
    {
        static $instance_counter = 0;
        $this->datumRelativeAddress = $datum_relative_address;
        if ('DESC' === strtoupper(trim($direction))) {
            $this->direction = 'DESC';
        }
        $instance_counter++;
        $this->indexTableAlias = 't' . md5(get_class($this) . ':' . $instance_counter);
    }

    public function updateQueryBuilder(QueryBuilder $query_builder): AbstractEntityListModifier
    {
        $datum = $this->getEntityList()->getEntityModel()->getFromRelativeAddress($this->datumRelativeAddress);
        if (false === $datum instanceof PlainTextDatum) {
            throw new Exception(''
                . 'Relative address ' . $this->datumRelativeAddress . ' '
                . 'in a ' . get_class($this->getEntityList()->getEntityModel()) . ' entity '
                . 'doesn\'t point to an instance of PlainTextDatum.')
            ;
        }
        $query_builder
            ->innerJoin('entity', $datum->getIndexTableName(), $this->getIndexTableAlias(), 'entity.id = ' . $this->getIndexTableAlias() . '.entity_id')
            ->addOrderBy($this->getIndexTableAlias() . '.value', $this->direction)
        ;
        return $this;
    }

    public function getDatumRelativeAddress(): string
    {
        return $this->datumRelativeAddress;
    }

    public function getDirection(): string
    {
        return $this->direction;
    }

    public function getIndexTableAlias(): string
    {
        return $this->indexTableAlias;
    }

}
