<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityListModifier;

use Doctrine\DBAL\Query\QueryBuilder;
use Exception;
use Vitya\CmsComponent\EntityComponent\PublicationEntityComponent;

class PublicationFilter extends AbstractEntityListModifier
{
    private $entityComponentRelativeAddress = '';
    private $indexTableAlias = '';
    private $mode = '';

    public function __construct(string $entity_component_relative_adress, string $mode = 'currently_published')
    {
        static $instance_counter = 0;
        $instance_counter++;
        $this->entityComponentRelativeAddress = $entity_component_relative_adress;
        $this->indexTableAlias = 't' . md5(get_class($this) . ':' . $instance_counter);
        $this->mode = $mode;
    }

    public function updateQueryBuilder(QueryBuilder $query_builder): AbstractEntityListModifier
    {
        $entity_component = $this->getEntityList()->getEntityModel()->getFromRelativeAddress($this->entityComponentRelativeAddress);
        if (false === $entity_component instanceof PublicationEntityComponent) {
            throw new Exception(''
                . 'Relative address ' . $this->entityComponentRelativeAddress . ' '
                . 'in a ' . get_class($this->getEntityList()->getEntityModel()) . ' entity '
                . 'doesn\'t point to an instance of PublicationEntityComponent.')
            ;
        }
        $query_builder->innerJoin(
            'entity', $entity_component->getIndexTableName(),
            $this->getIndexTableAlias(), 
            'entity.id = ' . $this->getIndexTableAlias() . '.entity_id')
        ;
        switch ($this->getMode()) {
            case 'currently_published':
                $query_builder
                    ->andWhere($this->getIndexTableAlias() . '.publishable')
                    ->andWhere($this->getIndexTableAlias() . '.start_uts <= :now')
                    ->andWhere($this->getIndexTableAlias() . '.end_uts > :now')
                    ->setParameter('now', time())
                ;
                break;
            case 'currently_not_published':
                $query_builder
                    ->andWhere(''
                        . '('
                        . '    NOT ' . $this->getIndexTableAlias() . '.publishable'
                        . ') OR ('
                        . '    ' . $this->getIndexTableAlias() . '.publishable'
                        . '    AND (' .  $this->getIndexTableAlias() . '.start_uts > :now'  . ' OR ' . $this->getIndexTableAlias() . '.end_uts <= :now' . ')'
                        . ')'
                    )
                    ->setParameter('now', time())
                ;
                break;
            case 'to_be_published':
                $query_builder
                    ->andWhere($this->getIndexTableAlias() . '.publishable')
                    ->andWhere($this->getIndexTableAlias() . '.start_uts > :now')
                    ->setParameter('now', time())
                ;
                break;
            case 'previously_published':
                $query_builder
                    ->andWhere($this->getIndexTableAlias() . '.publishable')
                    ->andWhere($this->getIndexTableAlias() . '.end_uts <= :now')
                    ->setParameter('now', time())
                ;
                break;
            case 'publishable':
                $query_builder->andWhere($this->getIndexTableAlias() . '.publishable');
                break;
            case 'not_publishable':
                $query_builder->andWhere('NOT ' . $this->getIndexTableAlias() . '.publishable');
                break;
            default:
                throw new Exception('Unrecognized mode: "' . $this->getMode() . '".');
        }
        return $this;
    }

    public function getEntityComponentRelativeAddress(): string
    {
        return $this->entityComponentRelativeAddress;
    }

    public function getIndexTableAlias(): string
    {
        return $this->indexTableAlias;
    }

    public function getMode(): string
    {
        return $this->mode;
    }

}
