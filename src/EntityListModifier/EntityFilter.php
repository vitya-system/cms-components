<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityListModifier;

use Doctrine\DBAL\Query\QueryBuilder;
use Exception;

class EntityFilter extends AbstractEntityListModifier
{
    private $property = '';
    private $operator = '';
    private $uts = 0;

    public function __construct(string $property, string $operator, int $uts)
    {
        if (false === in_array($property, ['creation_uts', 'last_modification_uts'])) {
            throw new Exception('Property can only be "creation_uts" or "last_modification_uts".');
        }
        if (false === in_array($operator, ['<', '<=', '=', '>=', '>'])) {
            throw new Exception('Property can only be "<", "<=", "=", ">=", ">".');
        }
        $this->property = $property;
        $this->operator = $operator;
        $this->uts = $uts;
    }

    public function updateQueryBuilder(QueryBuilder $query_builder): AbstractEntityListModifier
    {
        $query_builder->andWhere('entity.' . $this->property . ' ' . $this->operator . ' ' . $this->uts);
        return $this;
    }

    public function getProperty(): string
    {
        return $this->property;
    }

    public function getOperator(): string
    {
        return $this->operator;
    }

    public function getUts(): int
    {
        return $this->uts;
    }

}
