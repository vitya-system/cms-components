<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Db;

use Doctrine\DBAL\Connection;

class CmsDb
{
    private $prefix = '';
    private $connection = null;

    public function __construct(string $prefix, Connection $connection)
    {
        $this->prefix = $prefix;
        $this->connection = $connection;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function getConnection(): Connection
    {
        return $this->connection;
    }

 }
