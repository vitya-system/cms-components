<?php

/*
 * Copyright 2021, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Datum;

use Doctrine\DBAL\Schema\Schema;
use Exception;
use Vitya\CmsComponent\EntityComponent\EntityComponentInterface;
use Vitya\CmsComponent\IndexableInterface;

class PlainTextDatum extends AbstractDatum implements IndexableInterface
{
    const INDEX_VALUE_MAX_LENGTH = 256;
    const VALIDATION_ERROR_IS_MANDATORY = 2;

    private $value = '';

    public function __toString()
    {
        return $this->value;
    }

    public function modifySchema(Schema $schema): Schema
    {
        $options = $this->getOptions();
        if ($options['indexed']) {
            if ($this->isPartOfAStaticHierarchy()) { 
                $table = $schema->createTable($this->getIndexTableName());
                $table->addColumn('entity_id', 'integer', ['unsigned' => true]);
                $table->addColumn('value', 'string', ['length' => self::INDEX_VALUE_MAX_LENGTH]);
                $table->addIndex(['entity_id']);
                $table->addIndex(['value']);
            }
        }
        return $schema;
    }

    public function describe()
    {
        return 'string';
    }

    public function getDefaultOptions(): array
    {
        $default_options = [
            'mandatory' => false,
            'indexed' => false,
        ];
        return array_merge(parent::getDefaultOptions(), $default_options);
    }

    public function get()
    {
        return $this->value;
    }

    public function set($content): self
    {
        $this->value = (string) $content;
        $this->updateValidationState();
        return $this;
    }

    public function assertValidOptions(array $options): AbstractDatum
    {
        parent::assertValidOptions($options);
        if (false === is_bool($options['mandatory'])) {
            throw new Exception('Option "mandatory" must be defined as a boolean.');
        }
        if (false === is_bool($options['indexed'])) {
            throw new Exception('Option "indexed" must be defined as a boolean.');
        }
        return $this;
    }

    public function updateValidationState(): void
    {
        $options = $this->getOptions();
        $this->validationErrors = [];
        if ($options['mandatory'] && '' === $this->value) {
            $this->validationErrors[] = self::VALIDATION_ERROR_IS_MANDATORY;
        }
    }

    public function getIndexTableName(): string
    {
        $component_address = $this->getAddress();
        $entity_address = $this->getEntity()->getAddress();
        $relative_address = str_replace(' ' . $entity_address, '', ' ' . $component_address);
        $slug = str_replace('-', '_', str_replace('/', '_', $relative_address));
        return $this->getEntity()->getCmsDb()->getPrefix() . '_index_' . str_replace('-', '_', $this->getEntity()->getMachineName()) . $slug;
    }

    public function index(): void
    {
        if (false === $this->isPartOfAStaticHierarchy()) {
            return;
        }
        $options = $this->getOptions();
        if (false === $options['indexed']) {
            return;
        }
        $this->deleteFromIndex();
        $conn = $this->getEntity()->getCmsDb()->getConnection();
        $conn->insert(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
                'value' => mb_substr($this->value, 0, self::INDEX_VALUE_MAX_LENGTH),
            ]
        );
    }

    public function deleteFromIndex(): void
    {
        if (false === $this->isPartOfAStaticHierarchy()) {
            return;
        }
        $options = $this->getOptions();
        if (false === $options['indexed']) {
            return;
        }
        $conn = $this->getEntity()->getCmsDb()->getConnection();
        $conn->delete(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
            ]
        );
    }

    public function purgeIndex(): void
    {
        if (false === $this->isPartOfAStaticHierarchy()) {
            return;
        }
        $options = $this->getOptions();
        if (false === $options['indexed']) {
            return;
        }
        $conn = $this->getEntity()->getCmsDb()->getConnection();
        $conn->executeQuery('DELETE FROM ' . $this->getIndexTableName());
    }

}
