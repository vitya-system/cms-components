<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Datum;

use Exception;
use Vitya\CmsComponent\EntityComponent\EntityComponentInterface;

class HtmlTextDatum extends AbstractDatum
{
    const VALIDATION_ERROR_IS_MANDATORY = 1;

    private $value = '';

    public function __toString()
    {
        return $this->value;
    }

    public function describe()
    {
        return 'string';
    }

    public function getDefaultOptions(): array
    {
        $default_options = [
            'mandatory' => false,
        ];
        return array_merge(parent::getDefaultOptions(), $default_options);
    }

    public function get()
    {
        return $this->value;
    }

    public function set($content): self
    {
        $this->value = (string) $content;
        $this->updateValidationState();
        return $this;
    }

    public function assertValidOptions(array $options): AbstractDatum
    {
        parent::assertValidOptions($options);
        if (false === is_bool($options['mandatory'])) {
            throw new Exception('"Mandatory" option must be defined as a boolean.');
        }
        return $this;
    }

    public function updateValidationState(): void
    {
        $options = $this->getOptions();
        $plain_text_value = trim(html_entity_decode(strip_tags($this->value), ENT_QUOTES | ENT_SUBSTITUTE | ENT_HTML5, 'UTF-8'));
        $this->validationErrors = [];
        if ($options['mandatory'] && '' === $plain_text_value) {
            $this->validationErrors[] = self::VALIDATION_ERROR_IS_MANDATORY;
        }
    }

}
