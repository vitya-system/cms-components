<?php

/*
 * Copyright 2022, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Datum;

use Exception;
use Vitya\CmsComponent\EntityComponent\RefersToOtherEntitiesInterface;

class LinkedEntitiesDatum extends AbstractDatum implements RefersToOtherEntitiesInterface
{
    private $entities = [];

    public function __isset(string $name): bool
    {
        return 'entities' === $name;
    }

    public function __get(string $name)
    {
        if ('entities' === $name) {
            return $this->entities;
        }
        throw new Exception('Cannot find any accessible "' . $name . '" property.');
    }

    public function describe()
    {
        return ['entities' => 'array'];
    }

    public function getDefaultOptions(): array
    {
        $default_options = [
            'supported_classes' => [],
        ];
        return array_merge(parent::getDefaultOptions(), $default_options);
    }

    public function get()
    {
        return $this->entities;
    }

    public function set($content): static
    {
        if (false === is_array($content)) {
            return $this;
        }
        $sanitized_entities = [];
        foreach ($content as $k => $v) {
            if (false === is_string($v['class'])) {
                continue;
            }
            if (false === in_array($v['class'], $this->options['supported_classes'])) {
                continue;
            }
            if (false === is_int($v['id'])) {
                continue;
            }
            $sanitized_entities[] = [
                'class' => $v['class'],
                'id' => $v['id'],
            ];
        }
        $this->entities = $sanitized_entities;
        $this->updateValidationState();
        return $this;
    }

    public function getReferencesToOtherEntities(): array
    {
        return $this->entities;
    }

    public function assertValidOptions(array $options): static
    {
        parent::assertValidOptions($options);
        if (false === is_array($options['supported_classes'])) {
            throw new Exception('Supported classes must be defined as an array.');
        }
        foreach ($options['supported_classes'] as $supported_class) {
            if (false === is_string($supported_class)) {
                throw new Exception('Supported classes must be defined as an array of strings.');
            }
            if (false === in_array('Vitya\CmsComponent\Entity\EntityInterface', class_implements($supported_class))) {
                throw new Exception('"' . $supported_class . '" is not an entity class.');
            }
            $this->options['supported_classes'][] = $supported_class;
        }
        return $this;
    }

}
