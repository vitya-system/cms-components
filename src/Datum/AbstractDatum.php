<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Datum;

use Exception;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsComponent\EntityComponent\AbstractLeafEntityComponent;
use Vitya\Component\Authentication\UserInterface;

abstract class AbstractDatum extends AbstractLeafEntityComponent implements DatumInterface
{
    protected $options = [];

    public function __construct()
    {
        $this->setOptions([]);
    }

    public function getDefaultOptions(): array
    {
        return [
            'view_permission_name' => AbstractEntity::PERMISSION_VIEW,
            'modify_permission_name' => AbstractEntity::PERMISSION_MODIFY,
        ];
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): self
    {
        $options = array_merge($this->getDefaultOptions(), $options);
        $this->assertValidOptions($options);
        $this->options = $options;
        $this->updateValidationState();
        return $this;
    }

    public function canBeViewed(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && ('' !== $this->options['view_permission_name'])) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->options['view_permission_name'], $user);
        }
        return true;
    }

    public function canBeModified(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && ('' !== $this->options['modify_permission_name'])) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->options['modify_permission_name'], $user);
        }
        return true;
    }

    public function assertValidOptions(array $options): AbstractDatum
    {
        if (false === is_string($options['view_permission_name'])) {
            throw new Exception('View permission name must be defined as a (possibly empty) string.');
        }
        if (false === is_string($options['modify_permission_name'])) {
            throw new Exception('Modify permission name must be defined as a (possibly empty) string.');
        }
        return $this;
    }

    public function updateValidationState(): void
    {
        $this->validationErrors = [];
    }

}
