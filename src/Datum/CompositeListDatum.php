<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Datum;

use Exception;
use Throwable;
use Vitya\CmsComponent\Composite\CompositeFactoryInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;
use Vitya\CmsComponent\EntityComponent\AbstractEntityComponent;
use Vitya\Component\Authentication\UserInterface;

class CompositeListDatum extends AbstractEntityComponent implements DatumInterface
{
    private $compositeFactory = null;
    private $options = [];

    public function __construct(CompositeFactoryInterface $composite_factory)
    {
        $this->compositeFactory = $composite_factory;
    }

    public function getCompositeFactory(): CompositeFactoryInterface
    {
        return $this->compositeFactory;
    }

    public function getDefaultOptions(): array
    {
        return [
            'view_permission_name' => AbstractEntity::PERMISSION_VIEW,
            'modify_permission_name' => AbstractEntity::PERMISSION_MODIFY,
            'supported_classes' => [],
        ];
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): DatumInterface
    {
        $options = array_merge($this->getDefaultOptions(), $options);
        $this->assertValidOptions($options);
        $this->options = $options;
        $this->updateValidationState();
        return $this;
    }

    public function canBeViewed(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && ('' !== $this->options['view_permission_name'])) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->options['view_permission_name'], $user);
        }
        return true;
    }

    public function canBeModified(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && ('' !== $this->options['modify_permission_name'])) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->options['modify_permission_name'], $user);
        }
        return true;
    }

    public function get()
    {
        $composites = [];
        foreach ($this->getChildren() as $k => $v) {
            $composites[$k] = [
                'class' => get_class($v),
                'content' => $v->get(),
            ];
        }
        return $composites;
    }

    public function set($content): self
    {
        $this->removeAllChildren();
        $content = (array) $content;
        foreach ($content as $k => $v) {
            if (false === isset($v['class'])) {
                continue;
            }
            if (false === is_string($v['class'])) {
                continue;
            }
            if (false === in_array($v['class'], $this->getOptions()['supported_classes'])) {
                continue;
            }
            if (false === isset($v['content'])) {
                continue;
            }
            try {
                $composite = $this->getCompositeFactory()->makeComposite($v['class']);
                $composite->setPartOfAStaticHierarchy(false);
                foreach ($composite->getDescendants() as $descendant) {
                    $descendant->setPartOfAStaticHierarchy(false);
                }
                $this->addChild((string) $k, $composite);
                $composite->set($v['content']);
            } catch (Throwable $t) {
                // Do nothing.
                // This invalid element won't be added.
            }
        }
        $this->updateValidationState();
        return $this;
    }

    public function assertValidOptions(array $options): CompositeListDatum
    {
        if (false === is_array($options['supported_classes'])) {
            throw new Exception('Supported classes must be defined as an array.');
        }
        foreach ($options['supported_classes'] as $supported_class) {
            if (false === is_string($supported_class)) {
                throw new Exception('Supported classes must be defined as an array of strings.');
            }
            if (false === is_a($supported_class, 'Vitya\CmsComponent\Composite\AbstractComposite', true)) {
                throw new Exception('"' . $supported_class . '" must extend Vitya\CmsComponent\Composite\AbstractComposite.');
            }
            $this->options['supported_classes'][] = $supported_class;
        }
        return $this;
    }

    public function updateValidationState(): void
    {

    }

}
