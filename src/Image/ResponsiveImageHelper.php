<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Image;

use Exception;
use Vitya\Component\Image\ImageFactoryInterface;
use Vitya\Component\Image\ImageInterface;

class ResponsiveImageHelper
{
    private $source = null;
    private $imageFactory = null;

    public function __construct(ResponsiveImageSourceInterface $responsive_image_source, ImageFactoryInterface $image_factory)
    {
        $this->source = $responsive_image_source;
        $this->imageFactory = $image_factory;
    }

    public function getSource(): ResponsiveImageSourceInterface
    {
        return $this->source;
    }

    public function getImageFactory(): ImageFactoryInterface
    {
        return $this->imageFactory;
    }

    public function parseTransformationString(string $transformation_string): array
    {
        $transformation_description = [
            'target_aspect_ratio' => 0.0, // 0.0 means we keep the source image ratio.
            'mode' => 'noresize',
            'target_w' => 0,
            'target_h' => 0,
        ];
        $s = str_replace(' ', '', $transformation_string);
        $assertions = explode(';', $s);
        foreach ($assertions as $assertion) {
            $assertion_elements = explode('=', $assertion);
            if (count($assertion_elements) !== 2) {
                continue;
            }
            $k = $assertion_elements[0];
            $v = $assertion_elements[1];
            if ('mode' === $k) {
                if (in_array($v, ['min', 'max', 'ftw', 'fth', 'noresize'])) {
                    $transformation_description['mode'] = $v;
                } else {
                    throw new Exception('Unknown mode: "' . $v . '".');
                }
            }
            if ('aspect_ratio' === $k) {
                $elements = explode('/', $v);
                if (count($elements) === 1) {
                    $transformation_description['target_aspect_ratio'] = (float) $elements[0];
                } elseif (count($elements) === 2) {
                    $transformation_description['target_aspect_ratio'] = ((float) $elements[0]) / ((float) $elements[1]);
                }
            }
            if ('width' === $k) {
                $transformation_description['target_w'] = (int) $v;
            }
            if ('height' === $k) {
                $transformation_description['target_h'] = (int) $v;
            }
        }
        return $transformation_description;
    }

    /**
     * Returns an array describing how to resize and crop the source image
     * file.
     * 
     * This method will analyze the format description and determine how to
     * resize and crop the source image file.
     * 
     * @param float $target_aspect_ratio Desired aspect ratio. 0.0 means we 
     *     want to keep the original ratio.
     * @param string $mode Mode of operation. Can be: 'ftw' (fit to target
     *     width), 'fth' (fit to target height), 'min' (target width and
     *     height are the minimum dimensions of the final image), 'max' (target
     *     width and height are the maximum dimensions of the final image).
     * @param int $target_w Target width in pixels
     * @param int $target_h Target height in pixels
     */
    public function getOperations(
        string $mode,
        float $target_aspect_ratio,
        int $target_w, 
        int $target_h
    ): array
    {
        $source_aspect_ratio =
            (float) $this->getSource()->getResponsiveImageSourceWidth() 
            / (float) $this->getSource()->getResponsiveImageSourceHeight()
        ;
        if (0.0 === $target_aspect_ratio) {
            $target_aspect_ratio = $source_aspect_ratio;
        }
        // Sanitize parameters.
        if ('min' === $mode) {
            if (0 >= $target_w) {
                throw new Exception('Invalid target width.');
            }
            if (0 >= $target_h) {
                throw new Exception('Invalid target height.');
            }
        } elseif ('max' === $mode) {
            if (0 >= $target_w) {
                throw new Exception('Invalid target width.');
            }
            if (0 >= $target_h) {
                throw new Exception('Invalid target height.');
            }
        } elseif ('ftw' === $mode) {
            if (0 >= $target_w) {
                throw new Exception('Invalid target width.');
            }
            $target_h = 0;
        } elseif ('fth' === $mode) {
            $target_w = 0;
            if (0 >= $target_h) {
                throw new Exception('Invalid target height.');
            }
        } elseif ('noresize' === $mode) {
            $target_w = $this->getSource()->getResponsiveImageSourceWidth();
            $target_h = $this->getSource()->getResponsiveImageSourceHeight();
        } else {
            throw new Exception('Invalid mode = "' . $mode . '".');
        }
        // Determine operations.
        if ($source_aspect_ratio === $target_aspect_ratio) {
            $float_crop_zone_w = (float) $this->getSource()->getResponsiveImageSourceWidth();
            $float_crop_zone_h = (float) $this->getSource()->getResponsiveImageSourceHeight();
        } elseif ($source_aspect_ratio < $target_aspect_ratio) {
            $float_crop_zone_w = (float) $this->getSource()->getResponsiveImageSourceWidth();
            $float_crop_zone_h = (float) $this->getSource()->getResponsiveImageSourceWidth() / $target_aspect_ratio;
        } else {
            $float_crop_zone_w = (float) $this->getSource()->getResponsiveImageSourceHeight() * $target_aspect_ratio;
            $float_crop_zone_h = (float) $this->getSource()->getResponsiveImageSourceHeight();
        }
        $resize_factor = 1.0;
        switch ($mode) {
            case 'min':
                $h_factor = (float) $target_w / $float_crop_zone_w;
                $v_factor = (float) $target_h / $float_crop_zone_h;
                $resize_factor = max($h_factor, $v_factor);
                break;
            case 'max':
                $h_factor = (float) $target_w / $float_crop_zone_w;
                $v_factor = (float) $target_h / $float_crop_zone_h;
                $resize_factor = min($h_factor, $v_factor);
                break;
            case 'ftw':
                $resize_factor = (float) $target_w / $float_crop_zone_w;
                break;
            case 'fth':
                $resize_factor = (float) $target_h / $float_crop_zone_h;
                break;
            case 'noresize':
                $resize_factor = 1.0;
                break;
            default:
                throw new Exception('Unrecognized mode: "' . $mode . '".');
        }
        $float_resize_w = (float) $this->getSource()->getResponsiveImageSourceWidth() * $resize_factor;
        $float_resize_h = (float) $this->getSource()->getResponsiveImageSourceHeight() * $resize_factor;
        $resize_w = (int) $float_resize_w;
        $resize_h = (int) $float_resize_h;
        $float_crop_w = $resize_factor * $float_crop_zone_w;
        $float_crop_h = $resize_factor * $float_crop_zone_h;
        $crop_w = min((int) $float_crop_w, $this->getSource()->getResponsiveImageSourceWidth());
        $crop_h = min((int) $float_crop_h, $this->getSource()->getResponsiveImageSourceHeight());
        $crop_x = 0;
        $crop_y = 0;
        $autocrop = false;
        if ($this->getSource()->getResponsiveImageSourceAutocrop()) {
            $autocrop = true;
        } else {
            $float_crop_x = ($this->getSource()->getResponsiveImageSourceCoiX() * $float_resize_w) - 0.5 * $float_crop_w;
            $float_crop_y = ($this->getSource()->getResponsiveImageSourceCoiY() * $float_resize_h) - 0.5 * $float_crop_h;
            $crop_x = max((int) $float_crop_x, 0);
            $crop_y = max((int) $float_crop_y, 0);
            if ($crop_x + $crop_w > $resize_w) {
                $crop_x = $resize_w - $crop_w;
            }
            if ($crop_y + $crop_h > $resize_h) {
                $crop_y = $resize_h - $crop_h;
            }
        }
        $operations = [
            'resize_w' => $resize_w,
            'resize_h' => $resize_h,
            'crop_w' => $crop_w,
            'crop_h' => $crop_h,
            'crop_x' => $crop_x,
            'crop_y' => $crop_y,
            'autocrop' => $autocrop,
        ];
        return $operations;
    }

    public function getOutputFileFormat(string $input_mime_type, string $output_mime_type_hint): array
    {
        static $image_model = null;
        if (null === $image_model) {
            $image_model = $this->getImageFactory()->makeImage();
        }
        $supported_output_file_format = $image_model->getSupportedOutputFileFormats();
        if (false === isset($supported_output_file_format[$output_mime_type_hint])) {
            throw new Exception('Unsupported MIME type hint: "' . $output_mime_type_hint . '".');
        } 
        return $supported_output_file_format[$output_mime_type_hint];
    }

    public function makeImage(string $transformation_string): ImageInterface
    {
        $transformation_description = $this->parseTransformationString($transformation_string);
        $operations = $this->getOperations(
            $transformation_description['mode'],
            $transformation_description['target_aspect_ratio'],
            $transformation_description['target_w'], 
            $transformation_description['target_h']
        );
        $image = $this->getImageFactory()->makeImage();
        $image->loadAndResize($this->getSource()->getResponsiveImageSourceFilePath(), $operations['resize_w'], $operations['resize_h']);
        if ($operations['autocrop']) {
            $image->autocrop($operations['crop_w'], $operations['crop_h']);
        } else {
            $image->crop($operations['crop_w'], $operations['crop_h'], $operations['crop_x'], $operations['crop_y']);
        }
        return $image;
    }

}
