<?php

/*
 * Copyright 2020, 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Entity;

use Exception;
use Vitya\CmsComponent\AddressableObject\AddressableObjectInterface;
use Vitya\CmsComponent\Composite\CompositeFactoryInterface;
use Vitya\CmsComponent\Composite\CompositeInterface;
use Vitya\Component\Service\DependencyInjectorInterface;

class EntityFactory implements AddressableObjectInterface, EntityFactoryInterface, CompositeFactoryInterface
{
    private $availableEntityClassNames = [];
    private $collectionMachineNameMap = [];
    private $dependencyInjector = null;
    private $registry = [];

    public function __construct(DependencyInjectorInterface $dependency_injector, array $available_entity_class_names) {
        $this->dependencyInjector = $dependency_injector;
        $this->availableEntityClassNames = [];
        foreach ($available_entity_class_names as $class_name) {
            $this->availableEntityClassNames[$class_name::getMachineName()] = $class_name;
            $this->collectionMachineNameMap[$class_name::getCollectionMachineName()] = $class_name;
        }
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getRegistry(): array
    {
        return $this->registry;
    }

    public function getAddress(): string
    {
        return '/entities';
    }

    public function getFromRelativeAddress(string $relative_address): ?AddressableObjectInterface
    {
        $relative_address_elements = explode('/', $relative_address);
        if (count($relative_address_elements) < 2) {
            return null;
        }
        $entity = $this->load(
            $this->getClassNameFromCollectionMachineName($relative_address_elements[0]),
            (int) $relative_address_elements[1]
        );
        array_shift($relative_address_elements);
        array_shift($relative_address_elements);
        if (count($relative_address_elements) > 0) {
            return $entity->getFromRelativeAddress(implode('/', $relative_address_elements));
        } else {
            return $entity;
        }
    }

    public function make(string $class_name): EntityInterface
    {
        if (false === in_array($class_name, $this->availableEntityClassNames)) {
            throw new Exception('"' . $class_name . '" is not registered as an available entity class name.');
        }
        $interfaces = class_implements($class_name);
        if (false === isset($interfaces['Vitya\CmsComponent\Entity\EntityInterface'])) {
            throw new Exception('Class "' . $class_name . '" does not implement EntityInterface.');
        }
        $entity_model = $this->dependencyInjector->make($class_name);
        return $entity_model;
    }

    public function load(string $class_name, int $id): ?EntityInterface
    {
        $key = $class_name . ':' . (string) $id;
        if (isset($this->registry[$key])) {
            return $this->registry[$key];
        }
        $entity_model = $this->make($class_name);
        $entity = $entity_model->load($id);
        if (null !== $entity) {
            $this->registry[$key] = $entity;
        }
        return $entity;
    }

    public function loadMultiple(string $class_name, array $ids): array
    {
        $entities = [];
        $ids_to_load = [];
        foreach ($ids as $id) {
            if (false === is_int($id) || 0 > $id) {
                continue;
            }
            $key = $class_name . ':' . (string) $id;
            if (isset($this->registry[$key])) {
                $entities[$id] = $this->registry[$key];
            } else {
                $entities[$id] = null; 
                $ids_to_load[] = $id;
            }
        }
        if (empty($ids_to_load)) {
            return $entities;
        }
        $entity_model = $this->make($class_name);
        $additional_entities = $entity_model->loadMultiple($ids_to_load);
        foreach ($additional_entities as $id => $entity) {
            $key = $class_name . ':' . (string) $id;
            if (null !== $entity) {
                $this->registry[$key] = $entity;
                $entities[$id] = $entity;
            }
        }
        return $entities;
    }

    public function getAvailableEntityClassNames(): array
    {
        return $this->availableEntityClassNames;
    }

    public function getClassNameFromMachineName(string $machine_name): string
    {
        if (false === isset($this->availableEntityClassNames[$machine_name])) {
            throw new Exception('Machine name "' . $machine_name . '" has no matching class name.');
        }
        return $this->availableEntityClassNames[$machine_name];
    }

    public function getClassNameFromCollectionMachineName(string $collection_machine_name): string
    {
        if (false === isset($this->collectionMachineNameMap[$collection_machine_name])) {
            throw new Exception('Collection machine name "' . $collection_machine_name . '" has no matching class name.');
        }
        return $this->collectionMachineNameMap[$collection_machine_name];
    }

    public function makeComposite(string $class_name): CompositeInterface
    {
        $interfaces = class_implements($class_name);
        if (false === isset($interfaces['Vitya\CmsComponent\Composite\CompositeInterface'])) {
            throw new Exception('Class "' . $class_name . '" does not implement CompositeInterface.');
        }
        $composite = $this->dependencyInjector->make($class_name);
        return $composite;
    }

}
