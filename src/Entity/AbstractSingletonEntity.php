<?php

/*
 * Copyright 2021, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Entity;

use Exception;
use Throwable;
use Vitya\Component\Acl\Acl;
use Vitya\Component\Acl\AclInterface;
use Vitya\Component\Authentication\UserInterface;

abstract class AbstractSingletonEntity extends AbstractEntity
{
    public static function getMachineName(): string
    {
        return static::getCollectionMachineName();
    }

    public function getCmsCollectionAcl(): ?AclInterface
    {
        static $acl = null;
        if (null === $acl) {
            $acl = new Acl();
            $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_VIEW);
        }
        return $acl;
    }

    public function getCmsAcl(): ?AclInterface
    {
        static $acl = null;
        if (null === $acl) {
            $acl = new Acl();
            $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_VIEW);
            $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_MODIFY);
        }
        return $acl;
    }

    public function create(int $force_id = 0): self
    {
        if ($force_id !== 1) {
            throw new Exception('You cannot create a singleton with an id different than 1.');
        }
        return parent::create(1);
    }

    public function load(int $id): ?self
    {
        if ($id !== 1) {
            throw new Exception('You cannot load a singleton with an id different than 1.');
        }
        return parent::load($id);
    }

    public function save(): self
    {
        if ($this->getId() !== 1) {
            throw new Exception('The id of a singleton must be 1.');
        }
        return parent::save();
    }

    public function install(): void
    {
        parent::install();
        try {
            $this->create(1);
        } catch (Throwable $t) {
            // Singleton has probably already been created.
        }
    }

    public function canBeCreated(UserInterface $user = null): bool
    {
        return false;
    }

    public function canBeDeleted(UserInterface $user = null): bool
    {
        return false;
    }

}
