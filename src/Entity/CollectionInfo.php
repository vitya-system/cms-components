<?php

/*
 * Copyright 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Entity;

use Exception;
use Throwable;
use Vitya\CmsComponent\Db\CmsDb;

class CollectionInfo
{
    private $className = '';
    private $cmsDb = null;
    private $content = [];
    private $lastModificationUts = 0;

    public function __construct(CmsDb $cms_db)
    {
        $this->cmsDb = $cms_db;
    }

    public function getClassName(): string
    {
        return $this->className;
    }

    public function setClassName(string $class_name): CollectionInfo
    {
        $this->className = $class_name;
        return $this;
    }

    public function get(string $key)
    {
        if (isset($this->content[$key])) {
            return $this->content[$key];
        }
        return null;
    }

    public function set(string $key, $content): CollectionInfo
    {
        if (null === $content) {
            unset($this->content[$key]);
            return $this;
        }
        $this->content[$key] = $content;
        return $this;
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function getLastModificationUts(): int
    {
        return $this->lastModificationUts;
    }

    public function load(string $class_name): ?CollectionInfo
    {
        $new_collection_info = clone $this;
        $new_collection_info->className = $class_name;
        $new_collection_info->lastModificationUts = 0;
        $new_collection_info->content = [];
        $conn = $this->getCmsDb()->getConnection();
        $query_builder = $conn->createQueryBuilder();
        $query_builder
            ->select('last_modification_uts', 'content')
            ->from($this->cmsDb->getPrefix() . '_collection_info')
            ->where('class_name = :class_name')
            ->setParameter('class_name', $class_name)
        ;
        $result = $query_builder->executeQuery();
        if (false !== $row = $result->fetchAssociative()) {
            $new_collection_info->lastModificationUts = (int) $row['last_modification_uts'];
            $new_collection_info->content = json_decode($row['content'], true);
        } else {
            $conn->beginTransaction();
            try {
                $conn->insert(
                    $this->cmsDb->getPrefix() . '_collection_info',
                    [
                        'class_name' => $class_name,
                        'last_modification_uts' => $new_collection_info->lastModificationUts,
                        'content' => json_encode($new_collection_info->content, JSON_PRETTY_PRINT),
                    ]
                );
                $conn->commit();
            } catch (Throwable $t) {
                $conn->rollBack();
                throw $t;
            }
        }
        return $new_collection_info;
    }

    public function save(): CollectionInfo
    {
        $conn = $this->getCmsDb()->getConnection();
        $conn->beginTransaction();
        try {
            $now_uts = time();
            $conn->update(
                $this->cmsDb->getPrefix() . '_collection_info',
                [
                    'last_modification_uts' => $now_uts,
                    'content' => json_encode($this->content, JSON_PRETTY_PRINT),
                ],
                [
                    'class_name' => $this->className,
                ]
            );
            $conn->commit();
            $this->lastModificationUts = $now_uts;
        } catch (Throwable $t) {
            $conn->rollBack();
            throw $t;
        }
        return $this;
    }

}