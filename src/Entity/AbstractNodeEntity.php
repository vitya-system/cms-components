<?php

/*
 * Copyright 2023, 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Entity;

use Vitya\CmsComponent\Db\CmsDb;
use Vitya\CmsComponent\EntityComponent\AclEntityComponent;
use Vitya\CmsComponent\EntityComponent\EntityComponentFactory;
use Vitya\CmsComponent\Tree\TreeStructure;
use Vitya\Component\Acl\Acl;
use Vitya\Component\Acl\AclInterface;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\L10n\LocaleCatalog;
use Vitya\Component\Log\LoggerCollectionInterface;

abstract class AbstractNodeEntity extends AbstractEntity implements HasATreeStructureInterface
{
    private $acl = null;

    public function __construct(
        AuthenticationService $authn,
        CmsDb $cms_db,
        LocaleCatalog $locale_catalog,
        LoggerCollectionInterface $logger_collection,
        EntityComponentFactory $entity_component_factory
    ) {
        parent::__construct($authn, $cms_db, $locale_catalog, $logger_collection, $entity_component_factory);
        $acl_component = new AclEntityComponent(
            $authn,
            $cms_db,
            'cms',
            AbstractEntity::PERMISSION_VIEW,
            AbstractEntity::PERMISSION_MODIFY_ACL
        );
        $acl_component->setPossiblePermissions($this->getPossiblePermissions());
        $this->getRootComponent()->addChild('acl', $acl_component);
    }

    public function getPossiblePermissions(): array
    {
        return [
            AbstractEntity::PERMISSION_VIEW,
            AbstractEntity::PERMISSION_MODIFY,
            AbstractEntity::PERMISSION_MODIFY_ACL
        ];
    }

    public function getCmsCollectionAcl(): ?AclInterface
    {
        static $acl = null;
        if (null === $acl) {
            $acl = new Acl();
            $acl->addPermission(AclInterface::TYPE_ALL, 0, AbstractEntity::PERMISSION_VIEW);
            $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('editors'), AbstractEntity::PERMISSION_VIEW);
            $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('editors'), AbstractEntity::PERMISSION_MODIFY);
            $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_VIEW);
            $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_MODIFY);
        }
        return $acl;
    }

    public function getDefaultPermissions(UserInterface $creator = null): array
    {
        $default_permissions = [
            [AclInterface::TYPE_ALL, 0, AbstractEntity::PERMISSION_VIEW],
            [AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('editors'), AbstractEntity::PERMISSION_VIEW],
            [AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('editors'), AbstractEntity::PERMISSION_MODIFY],
            [AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_VIEW],
            [AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_MODIFY],
            [AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_MODIFY_ACL],
        ];
        if ($creator !== null) {
            $default_permissions = array_merge(
                $default_permissions,
                [
                    [AclInterface::TYPE_USER, $creator->getUserIdentifier(), AbstractEntity::PERMISSION_VIEW],
                    [AclInterface::TYPE_USER, $creator->getUserIdentifier(), AbstractEntity::PERMISSION_MODIFY],
                    [AclInterface::TYPE_USER, $creator->getUserIdentifier(), AbstractEntity::PERMISSION_MODIFY_ACL],
                ]
            );
        }
        return $default_permissions;
    }

    public function getCmsAcl(): ?AclInterface
    {
        return $this->getComponent('acl')->getComponentAcl();
    }

    public function preEntityCreate(): self
    {
        $acl = $this->getComponent('acl')->getComponentAcl();
        $creator = $this->getAuthenticationService()->getUser('cms');
        foreach ($this->getDefaultPermissions($creator) as $default_permission) { 
            $acl->addPermission($default_permission[0], $default_permission[1], $default_permission[2]);
        }
        return $this;
    }

    public function loadTreeStructure(): TreeStructure
    {
        $tree_structure = new TreeStructure();
        $collection_info = $this->getCollectionInfo();
        if (isset($collection_info['tree_structure'])) {
            $tree_structure->loadFromAssociativeMap($collection_info['tree_structure']);
        } else {
            $tree_structure->loadFromAssociativeMap([]);
        }
        return $tree_structure;
    }

    public function getTreeStructure(): TreeStructure
    {
        return $this->loadTreeStructure();
    }

    public function canBeReordered(UserInterface $user = null): bool
    {
        $collection_acl = $this->getCmsCollectionAcl();
        if (null === $collection_acl) {
            return true;
        }
        if (null !== $user && get_class($user) !== $this->getCmsUserProvider()->getSupportedUserClass()) {
            return false;
        }
        return $collection_acl->isGranted(AbstractEntity::PERMISSION_MODIFY, $user);
    }

}
