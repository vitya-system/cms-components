<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Entity;

use Vitya\CmsComponent\AddressableObject\AddressableObjectInterface;

interface EntityInterface extends AddressableObjectInterface
{
    public static function getCollectionMachineName(): string;
    
    public static function getMachineName(): string;

    public function getId(): int;

    public function duplicate(): EntityInterface;

    public function create(int $force_id = 0): EntityInterface;

    public function load(int $id): ?EntityInterface;

    public function loadMultiple(array $ids): array;

    public function save(): EntityInterface;

    public function delete(): EntityInterface;

    public function getIds(): array;

    public function getCount(): int;

    public function getTitle(): string;

    public function install(): void;

}
