<?php

/*
 * Copyright 2022, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Entity;

use Vitya\CmsComponent\Db\CmsDb;
use Vitya\CmsComponent\EntityComponent\EntityComponentFactory;
use Vitya\CmsComponent\EntityComponent\IdentityEntityComponent;
use Vitya\CmsComponent\EntityComponent\UserManagementEntityComponent;
use Vitya\Component\Acl\Acl;
use Vitya\Component\Acl\AclInterface;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Authentication\UserWithUsernameAndPasswordInterface;
use Vitya\Component\L10n\LocaleCatalog;
use Vitya\Component\Log\LoggerCollectionInterface;

abstract class AbstractUserEntity extends AbstractEntity implements UserInterface, UserWithUsernameAndPasswordInterface
{
    const PERMISSION_MODIFY_IDENTITY = 'i';
    const PERMISSION_MANAGE_USER = 'u';

    public function __construct(
        AuthenticationService $authn,
        CmsDb $cms_db,
        LocaleCatalog $locale_catalog,
        LoggerCollectionInterface $logger_collection,
        EntityComponentFactory $entity_component_factory
    ) {
        parent::__construct($authn, $cms_db, $locale_catalog, $logger_collection, $entity_component_factory);
        $identity_component = new IdentityEntityComponent($cms_db, AbstractEntity::PERMISSION_VIEW, self::PERMISSION_MODIFY_IDENTITY);
        $this->getRootComponent()->addChild('identity', $identity_component);
        $user_management_component = new UserManagementEntityComponent($authn, $cms_db, 'cms', AbstractEntity::PERMISSION_VIEW, self::PERMISSION_MANAGE_USER);
        $this->getRootComponent()->addChild('user_management', $user_management_component);
    }

    public function getUserIdentifier(): int
    {
        return $this->getId();
    }

    public function getGroups(): array
    {
        return $this->getUserManagementComponent()->getGroups();
    }

    public function isOmnipotent(): bool
    {
        return $this->getUserManagementComponent()->isOmnipotent();
    }

    public function getUsername(): string
    {
        return $this->getIdentityComponent()->getUsername();
    }

    public function checkPassword(string $clear_password): bool
    {
        return $this->getIdentityComponent()->checkPassword($clear_password);
    }

    public function hashPassword(string $clear_password): string
    {
        return $this->getIdentityComponent()->hashPassword($clear_password);
    }

    public function getCmsCollectionAcl(): ?AclInterface
    {
        static $acl = null;
        if (null === $acl) {
            $acl = new Acl();
            $acl->addPermission(AclInterface::TYPE_ALL, 0, AbstractEntity::PERMISSION_VIEW);
            $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_VIEW);
            $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_MODIFY);
        }
        return $acl;
    }

    public function getCmsAcl(): ?AclInterface
    {
        $acl = new Acl();
        $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_VIEW);
        $acl->addPermission(AclInterface::TYPE_USER, $this->getUserIdentifier(), AbstractEntity::PERMISSION_VIEW);
        if ($this->isOmnipotent()) {
            return $acl; // Only omnipotent users are allowed to do anything to omnipotent users.
        }
        $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), AbstractEntity::PERMISSION_MODIFY);
        $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), self::PERMISSION_MODIFY_IDENTITY);
        $acl->addPermission(AclInterface::TYPE_GROUP, $this->getCmsUserProvider()->getUserGroupIdentifierByName('webmasters'), self::PERMISSION_MANAGE_USER);
        $acl->addPermission(AclInterface::TYPE_USER, $this->getUserIdentifier(), AbstractEntity::PERMISSION_MODIFY);
        $acl->addPermission(AclInterface::TYPE_USER, $this->getUserIdentifier(), self::PERMISSION_MODIFY_IDENTITY);
        return $acl;
    }

    public function getIdentityComponent(): IdentityEntityComponent
    {
        return $this->getComponent('identity');
    }

    public function getUserManagementComponent(): UserManagementEntityComponent
    {
        return $this->getComponent('user_management');
    }

}
