<?php

/*
 * Copyright 2021, 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Entity;

use Doctrine\DBAL\Schema\Schema;
use Exception;
use Psr\Log\LoggerInterface;
use Throwable;
use Vitya\CmsComponent\AddressableObject\AddressableObjectInterface;
use Vitya\CmsComponent\CmsDbSchemaModifierInterface;
use Vitya\CmsComponent\Composite\CompositeInterface;
use Vitya\CmsComponent\Db\CmsDb;
use Vitya\CmsComponent\Describable\DescribableInterface;
use Vitya\CmsComponent\EntityComponent\EntityComponentFactory;
use Vitya\CmsComponent\EntityComponent\EntityComponentInterface;
use Vitya\CmsComponent\EntityComponent\L10nEntityComponent;
use Vitya\CmsComponent\EntityComponent\PublicationEntityComponent;
use Vitya\CmsComponent\EntityComponent\RefersToOtherEntitiesInterface;
use Vitya\CmsComponent\EntityComponent\RootEntityComponent;
use Vitya\CmsComponent\EntityListModifier\EntitySort;
use Vitya\CmsComponent\IndexableInterface;
use Vitya\Component\Acl\AclInterface;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Authentication\UserProviderInterface;
use Vitya\Component\L10n\LocaleCatalog;
use Vitya\Component\Log\LoggerCollectionInterface;

abstract class AbstractEntity implements 
    DescribableInterface, 
    EntityInterface, 
    CmsDbSchemaModifierInterface, 
    IndexableInterface, 
    RefersToOtherEntitiesInterface
{
    const PERMISSION_VIEW = 'r';
    const PERMISSION_MODIFY = 'w';
    const PERMISSION_MODIFY_ACL = 'a';

    private static $collectionRegister = [];

    private $authn = null;
    private $cmsDb = null;
    private $id = 0;
    private $fullyCreated = false;
    private $creationUts = 0;
    private $lastModificationUts = 0;
    private $localeCatalog = null;
    private $loggerCollection = null;
    private $options = [];
    private $rootComponent = null;
    private $entityComponentFactory = null;

    public function __construct(
        AuthenticationService $authn,
        CmsDb $cms_db,
        LocaleCatalog $locale_catalog,
        LoggerCollectionInterface $logger_collection,
        EntityComponentFactory $entity_component_factory,
    ) {
        $this->authn = $authn;
        $this->cmsDb = $cms_db;
        $this->creationUts = time();
        $this->lastModificationUts = $this->creationUts;
        $this->localeCatalog = $locale_catalog;
        $this->loggerCollection = $logger_collection;
        $this->entityComponentFactory = $entity_component_factory;
        $this->rootComponent = new RootEntityComponent();
        $this->rootComponent->setEntity($this);
        $this->setOptions($this->getOptions());
        // Components.
        if ($this->options['use_publication']) {
            $this->addPublicationComponents();
        }
        $this->getEntityComponentFactory()->createComponentsFromDescription($this->getRootComponent(), $this->getDataDescription());
    }

    public function __isset(string $name): bool
    {
        if (in_array($name, [
            'class_name',
            'id',
            'collection_machine_name',
            'machine_name',
            'fully_created',
            'creation_uts',
            'last_modification_uts',
        ])) {
            return true;
        }
        return $this->rootComponent->__isset($name);
    }

    public function __get(string $name)
    {
        if ('class_name' === $name) {
            return get_class($this);
        }
        if ('id' === $name) {
            return $this->id;
        }
        if ('collection_machine_name' === $name) {
            return $this->getCollectionMachineName();
        }
        if ('machine_name' === $name) {
            return $this->getMachineName();
        }
        if ('fully_created' === $name) {
            return $this->fullyCreated;
        }
        if ('creation_uts' === $name) {
            return $this->creationUts;
        }
        if ('last_modification_uts' === $name) {
            return $this->lastModificationUts;
        }
        return $this->rootComponent->__get($name);
    }

    // DescribableInterface implementation
    // -------------------------------------------------------------------------

    public function describe()
    {
        $d = [
            'class_name' => 'string',
            'id' => 'int',
            'collection_machine_name' => 'string',
            'machine_name' => 'string',
            'fully_created' => 'bool',
            'creation_uts' => 'int',
            'last_modification_uts' => 'int',
        ];
        $d = $d + $this->rootComponent->describe();
        return $d;
    }
    
    // EntityInterface implementation
    // -------------------------------------------------------------------------

    public function getAddress(): string
    {
        return '/entities/' . static::getCollectionMachineName() . '/' . $this->id;
    }

    public function getFromRelativeAddress(string $relative_address): ?AddressableObjectInterface
    {
        $address_elements = explode('/', $relative_address);
        try {
            $next_object = $this->getComponent(str_replace('-', '_', $address_elements[0]));
        } catch (Throwable $t) {
            $next_object = null;
        }
        if (count($address_elements) === 1) {
            return $next_object;
        }
        if (false === $next_object instanceof AddressableObjectInterface) {
            return null;
        }
        array_shift($address_elements);
        return $next_object->getFromRelativeAddress(implode('/', $address_elements));
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function duplicate(): self
    {
        $new_entity = clone $this;
        $new_root_component = $this->rootComponent->duplicate();
        $new_entity->rootComponent = $new_root_component;
        $new_root_component->setEntity($new_entity);
        return $new_entity;
    }

    public function create(int $force_id = 0): self
    {
        $this->preEntityCreate();
        $new_entity = $this->duplicate();
        $now_uts = time();
        $conn = $this->getCmsDb()->getConnection();
        $conn->beginTransaction();
        try {
            if ($this->isFullyCreated()) {
                if (false === $this->isInAValidState()) {
                    throw new Exception('Entity cannot be created because it is in an invalid state.');
                }
            }
            $insert_parameters = [
                'fully_created' => $new_entity->isFullyCreated(),
                'creation_uts' => $now_uts,
                'last_modification_uts' => $now_uts,
                'content' => json_encode($new_entity->getRootComponent()->get(), JSON_PRETTY_PRINT),
            ];
            if (0 !== $force_id) {
                $insert_parameters['id'] = $force_id;
            }
            $conn->insert(
                $this->getEntityTableName(), 
                $insert_parameters,
            );
            $new_entity->setId((int) $conn->lastInsertId());
            $new_entity->setCreationUts($now_uts);
            $new_entity->setLastModificationUts($now_uts);
            $new_entity->postEntityCreate($this);
            if ($this->isFullyCreated()) {
                $new_entity->index();
                $new_entity->saveCollectionInfo();
            }
            $conn->commit();
            $this->getLogger()->notice('Entity created.', $new_entity->getLogContext());
        } catch (Throwable $t) {
            $conn->rollBack();
            throw $t;
        }
        return $new_entity;
    }

    public function load(int $id): ?EntityInterface
    {
        if ($id <= 0) {
            throw new Exception('AbstractEntity::load() expects a strictly positive id.');
        }
        $conn = $this->getCmsDb()->getConnection();
        $query_builder = $conn->createQueryBuilder();
        $query_builder
            ->select('id', 'fully_created', 'creation_uts', 'last_modification_uts', 'content')
            ->from($this->getEntityTableName())
            ->where('id = :id')
            ->setParameter('id', $id)
        ;
        $result = $query_builder->executeQuery();
        if (false === $row = $result->fetchAssociative()) {
            return null;
        }
        $new_entity = $this->duplicate();
        $new_entity->preEntityLoad($id);
        $new_entity->setFromRow($row);
        $new_entity->postEntityLoad();
        return $new_entity;
    }

    public function loadMultiple(array $ids): array
    {
        $entities = [];
        $sanitized_ids = [];
        foreach ($ids as $id) {
            if (false === is_int($id) || 0 > $id) {
                continue;
            }
            $entities[$id] = null;
            $sanitized_ids[] = $id;
        }
        $conn = $this->getCmsDb()->getConnection();
        $query_builder = $conn->createQueryBuilder();
        $query_builder
            ->select('id', 'fully_created', 'creation_uts', 'last_modification_uts', 'content')
            ->from($this->getEntityTableName())
            ->where('id IN (' . implode(', ', $sanitized_ids) . ')')
        ;
        $result = $query_builder->executeQuery();
        while (false !== $row = $result->fetchAssociative()) {
            $id = (int) $row['id'];
            $new_entity = $this->duplicate();
            $new_entity->preEntityLoad($id);
            $new_entity->setFromRow($row);
            $new_entity->postEntityLoad();
            $entities[$id] = $new_entity;
        } 
        return $entities;
    }

    public function save(): EntityInterface
    {
        $this->preEntitySave();
        $now_uts = time();
        $conn = $this->getCmsDb()->getConnection();
        $conn->beginTransaction();
        try {
            if ($this->isFullyCreated()) {
                if (false === $this->isInAValidState()) {
                    throw new Exception('Entity cannot be saved because it is in an invalid state.');
                }
            }
            $conn->update(
                $this->getEntityTableName(), 
                [
                    'fully_created' => $this->isFullyCreated(),
                    'last_modification_uts' => $now_uts,
                    'content' => json_encode($this->getRootComponent()->get(), JSON_PRETTY_PRINT),
                ],
                [
                    'id' => $this->getId(),
                ]
            );
            $this->setLastModificationUts($now_uts);
            $this->postEntitySave();
            if ($this->isFullyCreated()) {
                $this->index();
                $this->saveCollectionInfo();
            }
            $conn->commit();
            $this->getLogger()->notice('Entity saved.', $this->getLogContext());
        } catch (Throwable $t) {
            $conn->rollBack();
            throw $t;
        }
        return $this;
    }

    public function delete(): EntityInterface
    {
        $this->preEntityDelete();
        $conn = $this->getCmsDb()->getConnection();
        $conn->beginTransaction();
        try {
            $conn->delete(
                $this->getEntityTableName(), 
                [
                    'id' => $this->getId(),
                ]
            );
            $this->postEntityDelete();
            $this->deleteFromIndex();
            $this->saveCollectionInfo();
            $conn->commit();
            $this->getLogger()->notice('Entity deleted.', $this->getLogContext());
        } catch (Throwable $t) {
            $conn->rollBack();
            throw $t;
        }
        return $this;
    }

    public function getIds(): array
    {
        $entity_list = $this->getEntityList([
            'exclude_fully_created_entities' => false,
            'exclude_non_fully_created_entities' => false,
        ]);
        $entity_list->addModifier(new EntitySort('id', 'ASC'));
        return $entity_list->getIds();
    }

    public function getCount(): int
    {
        $entity_list = $this->getEntityList([
            'exclude_fully_created_entities' => false,
            'exclude_non_fully_created_entities' => false,
        ]);
        return $entity_list->getCount();
    }

    public function getTitle(): string
    {
        return $this->getLocalizedTitle($this->getLocaleCatalog()->getDefaultLocale()->getId());
    }

    public function install(): void
    {
        $this->rootComponent->install();
    }

    // CmsDbSchemaModifierInterface implementation
    // -------------------------------------------------------------------------

    public function modifySchema(Schema $schema): Schema
    {
        $table = $schema->createTable($this->getEntityTableName());
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('fully_created', 'boolean');
        $table->addColumn('creation_uts', 'integer');
        $table->addColumn('last_modification_uts', 'integer');
        $table->addColumn('content', 'text');
        $table->setPrimaryKey(['id']);
        $table->addIndex(array('fully_created'));
        $table->addIndex(array('creation_uts'));
        $table->addIndex(array('last_modification_uts'));
        foreach ($this->rootComponent->getDescendants() as $descendant) {
            if ($descendant instanceof CmsDbSchemaModifierInterface) {
                $schema = $descendant->modifySchema($schema);
            }
        }
        return $schema;
    }

    // IndexableInterface implementation
    // -------------------------------------------------------------------------

    public function index(): void
    {
        foreach ($this->rootComponent->getDescendants() as $descendant) {
            if ($descendant instanceof IndexableInterface) {
                $descendant->index();
            }
        }
    }

    public function deleteFromIndex(): void
    {
        foreach ($this->rootComponent->getDescendants() as $descendant) {
            if ($descendant instanceof IndexableInterface) {
                $descendant->deleteFromIndex();
            }
        }
    }

    public function purgeIndex(): void
    {
        foreach ($this->rootComponent->getDescendants() as $descendant) {
            if ($descendant instanceof IndexableInterface) {
                $descendant->purgeIndex();
            }
        }
    }

    // RefersToOtherEntitiesInterface implementation
    // -------------------------------------------------------------------------

    public function getReferencesToOtherEntities(): array
    {
        $references = [];
        foreach ($this->rootComponent->getDescendants() as $descendant) {
            if ($descendant instanceof RefersToOtherEntitiesInterface) {
                $references = array_merge($references, $descendant->getReferencesToOtherEntities());
            }
        }
        return $references;
    }

    // AbstractEntity own methods
    // -------------------------------------------------------------------------

    public function getCmsCollectionAcl(): ?AclInterface
    {
        return null;
    }
    
    public function getCmsAcl(): ?AclInterface
    {
        return null;
    }

    public function getAuthenticationService(): AuthenticationService
    {
        return $this->authn;
    }

    public function getCmsUserProvider(): UserProviderInterface
    {
        return $this->authn->getUserProvider('cms');
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function getEntityComponentFactory(): EntityComponentFactory
    {
        return $this->entityComponentFactory;
    }

    public function setId(int $id): AbstractEntity
    {
        $this->id = $id;
        return $this;
    }

    public function isFullyCreated(): bool
    {
        return $this->fullyCreated;
    }

    public function setFullyCreated(bool $fully_created): AbstractEntity
    {
        $this->fullyCreated = $fully_created;
        return $this;
    }

    public function getCreationUts(): int
    {
        return $this->creationUts;
    }

    public function setCreationUts(int $creation_uts): AbstractEntity
    {
        $this->creationUts = $creation_uts;
        return $this;
    }

    public function getLastModificationUts(): int
    {
        return $this->lastModificationUts;
    }

    public function setLastModificationUts(int $last_modification_uts): AbstractEntity
    {
        $this->lastModificationUts = $last_modification_uts;
        return $this;
    }

    public function getLocaleCatalog(): LocaleCatalog
    {
        return $this->localeCatalog;
    }

    public function getLoggerCollection(): LoggerCollectionInterface
    {
        return $this->loggerCollection;
    }

    public function getRootComponent(): RootEntityComponent
    {
        return $this->rootComponent;
    }

    public function getComponent(string $id): EntityComponentInterface
    {
        return $this->rootComponent->getChild($id);
    }

    public function preEntityCreate(): self
    {
        $this->rootComponent->preEntityCreate();
        return $this;
    }

    public function preEntityLoad(int $id): self
    {
        $this->rootComponent->preEntityLoad($id);
        return $this;
    }

    public function preEntitySave(): self
    {
        $this->rootComponent->preEntitySave();
        return $this;
    }

    public function preEntityDelete(): self
    {
        $this->rootComponent->preEntityDelete();
        return $this;
    }

    public function postEntityCreate(AbstractEntity $original_entity): self
    {
        $this->rootComponent->postEntityCreate($original_entity->rootComponent);
        return $this;
    }

    public function postEntityLoad(): self
    {
        $this->rootComponent->postEntityLoad();
        return $this;
    }

    public function postEntitySave(): self
    {
        $this->rootComponent->postEntitySave();
        return $this;
    }

    public function postEntityDelete(): self
    {
        $this->rootComponent->postEntityDelete();
        return $this;
    }

    public function setFromRow(array $row): AbstractEntity
    {
        $this->setId((int) $row['id']);
        $this->setFullyCreated((bool) $row['fully_created']);
        $this->setCreationUts((int) $row['creation_uts']);
        $this->setLastModificationUts((int) $row['last_modification_uts']);
        $this->rootComponent->set(json_decode($row['content'], true));
        return $this;
    }

    public function getEntityTableName(): string
    {
        $sanitized_machine_name = str_replace('-', '_', $this->getMachineName());
        return $this->cmsDb->getPrefix() . '_entity_' . $sanitized_machine_name;
    }

    public function getDataDescription(): array
    {
        return [];
    }

    public function addPublicationComponents(): AbstractEntity
    {
        $l10n_component = new L10nEntityComponent($this->localeCatalog);
        $this->rootComponent->addChild('publication', $l10n_component);
        foreach ($this->localeCatalog->getLocales() as $locale) {
            $publication_component = new PublicationEntityComponent(
                $this->cmsDb,
                AbstractEntity::PERMISSION_VIEW,
                AbstractEntity::PERMISSION_MODIFY
            );
            $l10n_component->addChild($locale->getId(), $publication_component);
        }
        return $this;
    }

    public function getEntityList(array $base_options = []): EntityList
    {
        return new EntityList($this, $base_options);
    }

    public function canBeCreated(UserInterface $user = null): bool
    {
        $collection_acl = $this->getCmsCollectionAcl();
        if (null === $collection_acl) {
            return true;
        }
        if (null !== $user && get_class($user) !== $this->getCmsUserProvider()->getSupportedUserClass()) {
            return false;
        }
        return $collection_acl->isGranted(AbstractEntity::PERMISSION_MODIFY, $user);
    }

    public function canBeViewed(UserInterface $user = null): bool
    {
        $acl = $this->getCmsAcl();
        if (null === $acl) {
            return true;
        }
        if (null !== $user && get_class($user) !== $this->getCmsUserProvider()->getSupportedUserClass()) {
            return false;
        }
        return $acl->isGranted(AbstractEntity::PERMISSION_VIEW, $user);
    }

    public function canBeModified(UserInterface $user = null): bool
    {
        $acl = $this->getCmsAcl();
        if (null === $acl) {
            return true;
        }
        if (null !== $user && get_class($user) !== $this->getCmsUserProvider()->getSupportedUserClass()) {
            return false;
        }
        return $acl->isGranted(AbstractEntity::PERMISSION_MODIFY, $user);
    }

    public function canBeDeleted(UserInterface $user = null): bool
    {
        $acl = $this->getCmsAcl();
        if (null === $acl) {
            return true;
        }
        if (null !== $user && get_class($user) !== $this->getCmsUserProvider()->getSupportedUserClass()) {
            return false;
        }
        return $acl->isGranted(AbstractEntity::PERMISSION_MODIFY, $user);
    }

    public function canBeListed(UserInterface $user = null): bool
    {
        $collection_acl = $this->getCmsCollectionAcl();
        if (null === $collection_acl) {
            return true;
        }
        if (null !== $user && get_class($user) !== $this->getCmsUserProvider()->getSupportedUserClass()) {
            return false;
        }
        return $collection_acl->isGranted(AbstractEntity::PERMISSION_VIEW, $user);
    }

    public function isInAValidState(): bool
    {
        foreach ($this->getRootComponent()->getDescendants() as $descendant) {
            if (false === $descendant->isInAValidState()) {
                return false;
            }
        }
        return true;
    }

    public function getCollectionInfo(): CollectionInfo
    {
        $class_name = get_class($this);
        if (isset(self::$collectionRegister[$class_name])) {
            return self::$collectionRegister[$class_name];
        }
        $collection_info = new CollectionInfo($this->cmsDb);
        $collection_info = $collection_info->load($class_name);
        self::$collectionRegister[$class_name] = $collection_info;
        return $collection_info;
    }

    public function saveCollectionInfo(): void
    {
        $collection_info = $this->getCollectionInfo();
        $collection_info->save();
    }

    public function getLocalizedTitle(string $locale): string
    {
        return 'Untitled ' . $this->getMachineName() . ' #' . (string) $this->getId();
    }

    public function getDefaultOptions(): array
    {
        return [
            'use_publication' => false,
        ];
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setOptions(array $options): AbstractEntity
    {
        $options = array_merge($this->getDefaultOptions(), $options);
        $this->assertValidOptions($options);
        $this->options = $options;
        return $this;
    }

    public function assertValidOptions(array $options): AbstractEntity
    {
        if (isset($options['use_publication'])) {
            if (false === is_bool($options['use_publication'])) {
                throw new Exception('The use_publication option must be defined as a boolean.');
            }
        }
        return $this;
    }

    public function getLogger(): LoggerInterface
    {
        // Use the app default logger.
        return $this->loggerCollection->getLogger();
    }

    public function getLogContext(): array
    {
        return [
            'address' => $this->getAddress(),
            'is_fully_created' => $this->isFullyCreated(),
        ];
    }

}
