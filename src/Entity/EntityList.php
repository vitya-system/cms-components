<?php

/*
 * Copyright 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Entity;

use Doctrine\DBAL\Query\QueryBuilder;
use Vitya\Application\Helper\PaginationHelper;
use Vitya\CmsComponent\EntityListModifier\AbstractEntityListModifier;

class EntityList
{
    private $entityModel = null;
    private $baseOptions = [];
    private $firstResult = 0;
    private $maxResults = -1;
    private $modifiers = [];

    public function __construct(AbstractEntity $entity_model, array $base_options = [])
    {
        $this->entityModel = $entity_model;
        $this->setBaseOptions($base_options);
    }

    public function getEntityModel(): AbstractEntity
    {
        return $this->entityModel;
    }

    public function getBaseOptions(): array
    {
        return $this->baseOptions;
    }

    public function setBaseOptions(array $base_options): EntityList
    {
        $sanitized_base_options = [
            'exclude_fully_created_entities' => false,
            'exclude_non_fully_created_entities' => true,
        ];
        if (isset($base_options['exclude_fully_created_entities']) && is_bool($base_options['exclude_fully_created_entities'])) {
            $sanitized_base_options['exclude_fully_created_entities'] = $base_options['exclude_fully_created_entities'];
        }
        if (isset($base_options['exclude_non_fully_created_entities']) && is_bool($base_options['exclude_non_fully_created_entities'])) {
            $sanitized_base_options['exclude_non_fully_created_entities'] = $base_options['exclude_non_fully_created_entities'];
        }
        $this->baseOptions = $sanitized_base_options;
        return $this;
    }

    public function getFirstResult(): int
    {
        return $this->firstResult;
    }

    public function setFirstResult(int $i): EntityList
    {
        $this->firstResult = $i;
        return $this;
    }

    public function getMaxResults(): int
    {
        return $this->maxResults;
    }

    public function setMaxResults(int $n): EntityList
    {
        $this->maxResults = $n;
        return $this;
    }

    public function getModifiers(): array
    {
        return $this->modifiers;
    }

    public function addModifier(AbstractEntityListModifier $modifier): EntityList
    {
        $modifier->setEntityList($this);
        $this->modifiers[] = $modifier;
        return $this;
    }

    public function createQueryBuilder(): QueryBuilder
    {
        $conn = $this->entityModel->getCmsDb()->getConnection();
        $query_builder = $conn->createQueryBuilder()->from($this->entityModel->getEntityTableName(), 'entity');
        if ($this->baseOptions['exclude_fully_created_entities']) {
            $query_builder->andWhere('entity.fully_created <> 1');
        }
        if ($this->baseOptions['exclude_non_fully_created_entities']) {
            $query_builder->andWhere('entity.fully_created = 1');
        }
        foreach ($this->modifiers as $modifier) {
            $modifier->updateQueryBuilder($query_builder);
        }
        $query_builder->setFirstResult($this->firstResult);
        if ($this->maxResults > 0) {
            $query_builder->setMaxResults($this->maxResults);
        }
        return $query_builder;
    }

    public function getIds(): array
    {
        $query_builder = $this->createQueryBuilder();
        $result = $query_builder->select('id')->executeQuery();
        $ids = [];
        while (false !== $row = $result->fetchAssociative()) {
            $ids[] = (int) $row['id'];
        }
        return $ids;
    }

    public function getCount(): int
    {
        $query_builder = $this->createQueryBuilder();
        $result = $query_builder->select('COUNT(id) AS cid')->executeQuery();
        if (false !== $row = $result->fetchAssociative()) {
            return (int) $row['cid'];
        }
        return 0;
    }

    public function applyPagination(PaginationHelper $pagination): EntityList
    {
        $this->setFirstResult($pagination->getFirstItemOffset());
        $this->setMaxResults($pagination->getNbItemsPerPage());
        return $this;
    }

}
