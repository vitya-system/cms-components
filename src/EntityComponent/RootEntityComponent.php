<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Exception;
use Throwable;
use Vitya\CmsComponent\AddressableObject\AddressableObjectInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;


class RootEntityComponent extends AbstractEntityComponent
{
    private $entity = null;

    public function getAddress(): string
    {
        $base_address = '';
        $entity = $this->getEntity();
        if (null !== $entity) {
            $base_address = $entity->getAddress();
        }
        return $base_address;
    }

    public function getFromRelativeAddress(string $relative_address): ?AddressableObjectInterface
    {
        $address_elements = explode('/', $relative_address);
        try {
            $next_object = $this->getChild(str_replace('-', '_', $address_elements[0]));
        } catch (Throwable $t) {
            $next_object = null;
        }
        if (count($address_elements) === 1) {
            return $next_object;
        }
        if (false === $next_object instanceof AddressableObjectInterface) {
            return null;
        }
        array_shift($address_elements);
        return $next_object->getFromRelativeAddress(implode('/', $address_elements));
    }

    public function getEntity(): AbstractEntity
    {
        $this->assertValidEntity();
        return $this->entity;
    }

    public function getParent(): EntityComponentInterface
    {
        throw new Exception('A root entity component have no parent.');
    }

    public function setParent(EntityComponentInterface $entity_component): self
    {
        throw new Exception('A root entity component cannot have a parent.');
    }

    public function removeParent(): self
    {
        throw new Exception('A root entity component cannot have a parent.');
    }

    public function setEntity(AbstractEntity $entity): self
    {
        $this->entity = $entity;
        return $this;
    }

    public function getId(): string
    {
        return '';
    }

    public function setId(string $id): self
    {
        throw new Exception('A root entity always has an empty string as id.');
    }

    public function assertValidEntity(): void
    {
        if (null === $this->entity) {
            throw new Exception('No parent entity was set.');
        }
    }

}
