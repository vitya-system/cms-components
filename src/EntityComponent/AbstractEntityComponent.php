<?php

/*
 * Copyright 2021, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Doctrine\DBAL\Schema\Schema;
use Exception;
use Iterator;
use Throwable;
use Vitya\CmsComponent\AddressableObject\AddressableObjectInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;

abstract class AbstractEntityComponent implements Iterator, EntityComponentInterface
{
    private $children = [];
    private $parent = null;
    private $id = '';
    private $position = null;
    protected $validationErrors = [];
    private $partOfAStaticHierarchy = true;

    public function rewind(): void {
        $keys = array_keys($this->children);
        if (isset($keys[0])) {
            $this->position = (string) $keys[0];
            return;
        }
        $this->position = null;
    }

    public function current(): mixed {
        if (null === $this->position) {
            return null;
        }
        if (isset($this->children[$this->position])) {
            return $this->children[$this->position];
        }
        return null;
    }

    public function key(): ?string {
        return $this->position;
    }

    public function next(): void {
        if (null === $this->position) {
            return;
        }
        $keys = array_keys($this->children);
        $current_key_index = array_search($this->position, $keys);
        if (false === $current_key_index) {
            $this->position = null;
            return;
        }
        $current_key_index++;
        if (false === isset($keys[$current_key_index])) {
            $this->position = null;
            return;
        }
        $this->position = (string) $keys[$current_key_index];
    }

    public function valid(): bool {
        if (null === $this->position) {
            return false;
        }
        return isset($this->children[$this->position]);
    }

    public function __isset(string $name): bool
    {
        return isset($this->children[$name]);
    }

    public function __get(string $name)
    {
        if (isset($this->children[$name])) {
            return $this->children[$name];
        }
        throw new Exception('Cannot find any accessible "' . $name . '" property.');
    }

    public function getAddress(): string
    {
        $base_address = '';
        $parent = $this->getParent();
        if (null !== $parent) {
            $base_address = $parent->getAddress();
        }
        return $base_address . '/' . str_replace('_', '-', $this->id);
    }

    public function getFromRelativeAddress(string $relative_address): ?AddressableObjectInterface
    {
        $address_elements = explode('/', $relative_address);
        try {
            $next_object = $this->getChild(str_replace('-', '_', $address_elements[0]));
        } catch (Throwable $t) {
            $next_object = null;
        }
        if (count($address_elements) === 1) {
            return $next_object;
        }
        if (false === $next_object instanceof AddressableObjectInterface) {
            return null;
        }
        array_shift($address_elements);
        return $next_object->getFromRelativeAddress(implode('/', $address_elements));
    }

    public function modifySchema(Schema $schema): Schema
    {
        return $schema;
    }

    public function describe()
    {
        $d = [];
        foreach ($this->children as $k => $v) {
            $d[$k] = $v->describe();
        }
        return $d;
    }

    public function getEntity(): AbstractEntity
    {
        $this->assertValidParent();
        return $this->parent->getEntity();
    }

    public function duplicate(): self
    {
        $new_entity_component = clone $this;
        foreach ($new_entity_component->children as $id => $child) {
            $new_child = $child->duplicate();
            $new_entity_component->replaceChild($id, $new_child);
        }
        return $new_entity_component;
    }

    public function preEntityCreate(): self
    {
        foreach ($this->children as $id => $child) {
            $child->preEntityCreate();
        }
        return $this;
    }

    public function preEntityLoad(int $id): self
    {
        foreach ($this->children as $child_id => $child) {
            $child->preEntityLoad($id);
        }
        return $this;
    }

    public function preEntitySave(): self
    {
        foreach ($this->children as $id => $child) {
            $child->preEntitySave();
        }
        return $this;
    }

    public function preEntityDelete(): self
    {
        foreach ($this->children as $id => $child) {
            $child->preEntityDelete();
        }
        return $this;
    }

    public function postEntityCreate(EntityComponentInterface $source_component): self
    {
        foreach ($this->children as $id => $child) {
            $child->postEntityCreate($source_component->getChild($id));
        }
        return $this;
    }

    public function postEntityLoad(): self
    {
        foreach ($this->children as $id => $child) {
            $child->postEntityLoad();
        }
        return $this;
    }

    public function postEntitySave(): self
    {
        foreach ($this->children as $id => $child) {
            $child->postEntitySave();
        }
        return $this;
    }

    public function postEntityDelete(): self
    {
        foreach ($this->children as $id => $child) {
            $child->postEntityDelete();
        }
        return $this;
    }

    public function get()
    {
        $value = [];
        foreach ($this->children as $id => $child) {
            $value[$id] = $child->get();
        }
        return $value;
    }

    public function set($content): self
    {
        if (is_array($content)) {
            foreach ($this->children as $id => $child) {
                if (isset($content[$id])) {
                    $child->set($content[$id]);
                }
            }
        }
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->assertValidId($id);
        $this->id = $id;
        return $this;
    }

    public function getParent(): EntityComponentInterface
    {
        $this->assertValidParent();
        return $this->parent;
    }

    public function setParent(EntityComponentInterface $entity_component): self
    {
        $this->parent = $entity_component;
        return $this;
    }

    public function removeParent(): self
    {
        $this->parent = null;
        return $this;
    }

    public function addChild(string $id, EntityComponentInterface $entity_component): self
    {
        if (isset($this->children[$id])) {
            throw new Exception('An entity component named "' . $id . '" is already present.');
        }
        $entity_component->setId($id);
        $entity_component->setParent($this);
        $this->children[$id] = $entity_component;
        return $this;
    }

    public function replaceChild(string $id, EntityComponentInterface $entity_component): self
    {
        if (false === isset($this->children[$id])) {
            throw new Exception('There is no entity component named "' . $id . '".');
        }
        $entity_component->setId($id);
        $entity_component->setParent($this);
        $this->children[$id] = $entity_component;
        return $this;
    }

    public function removeChild(string $id): self
    {
        if (false === isset($this->children[$id])) {
            throw new Exception('There is no entity component named "' . $id . '".');
        }
        unset($this->children[$id]);
        return $this;
    }

    public function removeAllChildren(): self
    {
        foreach ($this->children as $child) {
            $child->removeParent();
        }
        $this->children = [];
        return $this;
    }

    public function getChildren(): array
    {
        return $this->children;
    }

    public function getDescendants(): array
    {
        $descendants = [];
        foreach ($this->children as $child) {
            $descendants[] = $child;
            $descendants = array_merge($descendants, $child->getDescendants());
        }
        return $descendants;
    }

    public function getChild(string $id): EntityComponentInterface
    {
        if (false === isset($this->children[$id])) {
            throw new Exception('Unknown entity component: "' . $id . '".');
        }
        return $this->children[$id];
    }

    public function hasChild(string $id): bool
    {
        return isset($this->children[$id]);
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function isInAValidState(): bool
    {
        return empty($this->validationErrors);
    }

    public function isPartOfAStaticHierarchy(): bool
    {
        return $this->partOfAStaticHierarchy;
    }

    public function install(): void
    {
        foreach ($this->children as $child) {
            $child->install();
        }
    }

    public function setPartOfAStaticHierarchy(bool $part_of_a_static_hierarchy): self
    {
        $this->partOfAStaticHierarchy = $part_of_a_static_hierarchy;
        return $this;
    }

    public function assertValidId(string $id): void
    {
        if (0 === preg_match('/^[a-z][a-z0-9_]*$/', $id)) {
            throw new Exception('"' . $id . '" is not a valid entity component id. It must only contain lowercase letters, digits or underscores, and begin with a letter.');
        }
    }

    public function assertValidParent(): void
    {
        if (null === $this->parent) {
            throw new Exception('No parent component was set.');
        }
    }

}
