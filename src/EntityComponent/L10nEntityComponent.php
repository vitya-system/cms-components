<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Exception;
use Vitya\Component\L10n\LocaleCatalog;

class L10nEntityComponent extends AbstractEntityComponent
{
    private $localeCatalog = null;

    public function __construct(LocaleCatalog $locale_catalog)
    {
        $this->localeCatalog = $locale_catalog;
    }

    public function addChild(string $id, EntityComponentInterface $entity_component): self
    {
        if (false === $this->localeCatalog->hasLocale($id)) {
            throw new Exception('Locale "' . $id . '" is not defined in the locale catalog.');
        }
        parent::addChild($id, $entity_component);
        return $this;
    }

    public function getLocaleCatalog(): LocaleCatalog
    {
        return $this->localeCatalog;
    }

}
