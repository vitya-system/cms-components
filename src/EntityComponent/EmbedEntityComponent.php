<?php

/*
 * Copyright 2022, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Doctrine\DBAL\Schema\Schema;
use Exception;
use Vitya\CmsComponent\Db\CmsDb;
use Vitya\CmsComponent\IndexableInterface;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\FileStorage\LocalFileStorageInterface;

class EmbedEntityComponent extends AbstractLeafEntityComponent implements IndexableInterface
{
    private $cmsDb = null;
    private $fileStorage = null;
    private $viewPermissionName = null;
    private $modifyPermissionName = null;
    private $content = null;

    public function __construct(
        CmsDb $cms_db,
        LocalFileStorageInterface $file_storage,
        string $view_permission_name = null,
        string $modify_permission_name = null
    ) {
        $this->cmsDb = $cms_db;
        $this->fileStorage = $file_storage;
        $this->viewPermissionName = $view_permission_name;
        $this->modifyPermissionName = $modify_permission_name;
        $this->content = $this->getDefaultContent();
    }

    public function __isset(string $name): bool
    {
       return in_array($name, array_keys($this->getDefaultContent()));
    }

    public function __get(string $name)
    {
        if (isset($this->content[$name])) {
            return $this->content[$name];
        }
        throw new Exception('Cannot find any accessible "' . $name . '" property.');
    }

    public function modifySchema(Schema $schema): Schema
    {
        $table = $schema->createTable($this->getIndexTableName());
        $table->addColumn('entity_id', 'integer', ['unsigned' => true]);
        $table->addIndex(['entity_id']);
        return $schema;
    }

    public function describe()
    {
        $description = [];
        foreach ($this->getDefaultContent() as $k => $v) {
            if (is_string($v)) {
                $description[$k] = 'string';
            } elseif (is_int($v)) {
                $description[$k] = 'int';
            } elseif (is_float($v)) {
                $description[$k] = 'float';
            }
        }
        return $description;
    }

    public function index(): void
    {
        $this->deleteFromIndex();
        $conn = $this->cmsDb->getConnection();
        $r = $conn->insert(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
            ]
        );
    }

    public function deleteFromIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->delete(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
            ]
        );
    }

    public function purgeIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->executeQuery('DELETE FROM ' . $this->getIndexTableName());
    }

    public function get()
    {
        return $this->content;
    }

    public function set($content): self
    {
        if (false === is_array($content)) {
            throw new Exception('EmbedEntityComponent::set() requires an array of properties.');
        }
        $new_content = $this->getDefaultContent();
        foreach ($new_content as $k => $v) {
            if (isset($content[$k])) {
                if (is_scalar($content[$k])) {
                    $new_v = $content[$k];
                    settype($new_v, gettype($new_content[$k]));
                    $new_content[$k] = $new_v;
                } else {
                    throw new Exception('Invalid type for "' . $k . '" property.');
                }
            }
        }
        $this->content = $new_content;
        $this->updateValidationState();
        return $this;
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function getViewPermissionName(): ?string
    {
        return $this->viewPermissionName;
    }

    public function setViewPermissionName(string $view_permission_name = null): EmbedEntityComponent
    {
        $this->viewPermissionName = $view_permission_name;
        return $this;
    }

    public function getModifyPermissionName(): ?string
    {
        return $this->modifyPermissionName;
    }

    public function setModifyPermissionName(string $modify_permission_name = null): EmbedEntityComponent
    {
        $this->modifyPermissionName = $modify_permission_name;
        return $this;
    }

    public function getFileStorage(): LocalFileStorageInterface
    {
        return $this->fileStorage;
    }

    public function getIndexTableName(): string
    {
        $component_address = $this->getAddress();
        $entity_address = $this->getEntity()->getAddress();
        $relative_address = str_replace(' ' . $entity_address, '', ' ' . $component_address);
        $slug = str_replace('-', '_', str_replace('/', '_', $relative_address));
        return $this->cmsDb->getPrefix() . '_index_' . str_replace('-', '_', $this->getEntity()->getMachineName()) . $slug;
    }

    public function canBeViewed(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->viewPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->viewPermissionName, $user);
        }
        return true;
    }

    public function canBeModified(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->modifyPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->modifyPermissionName, $user);
        }
        return true;
    }

    public function updateValidationState(): void
    {
        $this->validationErrors = [];
    }

    public function getDefaultContent(): array
    {
        return [
            'source_url' => '',
            'title' => '',
            'description' => '',
            'url' => '',
            'keywords' => '',
            'image' => '',
            'html' => '',
            'width' => 0,
            'height' => 0,
            'ratio' => 0.0,
            'author_name' => '',
            'author_url' => '',
            'cms' => '',
            'language' => '',
            'languages' => '',
            'provider_name' => '',
            'provider_url' => '',
            'icon' => '',
            'favicon' => '',
            'published_time' => '',
            'license' => '',
            'feeds' => '',
        ];
    }

}
