<?php

/*
 * Copyright 2023, 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Vitya\CmsComponent\Db\CmsDb;
use Vitya\Component\FileStorage\LocalFileStorageInterface;
use Vitya\Component\Image\ImageFactoryInterface;
use Vitya\Component\TemporaryStorage\TemporaryStorageInterface;

class ImageEntityComponent extends FileEntityComponent
{
    public function __construct(
        CmsDb $cms_db,
        LocalFileStorageInterface $file_storage,
        ImageFactoryInterface $image_factory,
        TemporaryStorageInterface $temporary_storage,
        string $view_permission_name = null,
        string $modify_permission_name = null,
        string $responsive_image_formats_json_path = ''
    ) {
        parent::__construct($cms_db, $file_storage, $image_factory, $temporary_storage, $view_permission_name, $modify_permission_name, $responsive_image_formats_json_path);
        $this->content = [
            'autocrop' => true,
            'coi_x' => 0.5,
            'coi_y' => 0.5,
        ] + $this->content;
    }

    public function __isset(string $name): bool
    {
        if (in_array($name, ['autocrop', 'coi_x', 'coi_y'])) {
            return true;
        }
        return parent::__isset($name);
    }

    public function __get(string $name)
    {
        if ('autocrop' === $name) {
            return $this->get()['autocrop'];
        }
        if ('coi_x' === $name) {
            return $this->get()['coi_x'];
        }
        if ('coi_y' === $name) {
            return $this->get()['coi_y'];
        }
        return parent::__get($name);
    }

    public function describe()
    {
        return [
            'autocrop' => 'bool',
            'coi_x' => 'float',
            'coi_y' => 'float',
        ] + parent::describe();
    }

    public function set($content): self
    {
        parent::set($content);
        $this->content = [
            'autocrop' => true,
            'coi_x' => 0.5,
            'coi_y' => 0.5,
        ] + $this->content;
        if (isset($content['autocrop']) && is_bool($content['autocrop'])) {
            $this->content['autocrop'] = $content['autocrop'];
        }
        $coi_x = (float) $content['coi_x']; 
        if ($coi_x >= 0.0 && $coi_x <= 1.0) {
            $this->content['coi_x'] = $coi_x;
        }
        $coi_y = (float) $content['coi_y']; 
        if ($coi_y >= 0.0 && $coi_y <= 1.0) {
            $this->content['coi_y'] = $coi_y;
        }
        return $this;
    }

    public function getFilePath(): string
    {
        $component_relative_address = (str_replace($this->getEntity()->getAddress() . '/', '', $this->getAddress()));
        $file_name = str_replace('/', '_', $component_relative_address) . '_' . $this->getEntity()->getId();
        return ''
            . '/ImageEntityComponent'
            . '/' . $this->getEntity()->getCollectionMachineName()
            . '/' . floor($this->getEntity()->getId() / 100)
            . '/' . $file_name
        ;
    }

    public function resetContentMetadata(): self
    {
        parent::resetContentMetadata();
        $this->content['autocrop'] = true;
        $this->content['coi_x'] = 0.5;
        $this->content['coi_y'] = 0.5;
        return $this;
    }

    // ResponsiveImageSourceInterface implementation
    // -------------------------------------------------------------------------

    public function getResponsiveImageSourceAutocrop(): bool
    {
        return (bool) $this->content['autocrop'];
    }

    public function getResponsiveImageSourceCoiX(): float
    {
        return (float) $this->content['coi_x'];
    }

    public function getResponsiveImageSourceCoiY(): float
    {
        return (float) $this->content['coi_y'];
    }

}
