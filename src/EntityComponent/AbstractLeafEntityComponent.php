<?php

/*
 * Copyright 2021, 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Doctrine\DBAL\Schema\Schema;
use Exception;
use Vitya\CmsComponent\AddressableObject\AddressableObjectInterface;
use Vitya\CmsComponent\Entity\AbstractEntity;

abstract class AbstractLeafEntityComponent implements EntityComponentInterface
{
    private $parent = null;
    private $id = '';
    protected $validationErrors = [];
    private $partOfAStaticHierarchy = true;

    public function getAddress(): string
    {
        $base_address = '';
        $parent = $this->getParent();
        if (null !== $parent) {
            $base_address = $parent->getAddress();
        }
        return $base_address . '/' . str_replace('_', '-', $this->id);
    }

    public function getFromRelativeAddress(string $relative_address): ?AddressableObjectInterface
    {
        return null;
    }

    public function modifySchema(Schema $schema): Schema
    {
        return $schema;
    }

    public function describe()
    {
        return 'void';
    }

    public function getEntity(): AbstractEntity
    {
        $this->assertValidParent();
        return $this->parent->getEntity();
    }

    public function duplicate(): AbstractLeafEntityComponent
    {
        $new_entity_component = clone $this;
        return $new_entity_component;
    }

    public function preEntityCreate(): AbstractLeafEntityComponent
    {
        return $this;
    }

    public function preEntityLoad(int $id): AbstractLeafEntityComponent
    {
        return $this;
    }

    public function preEntitySave(): AbstractLeafEntityComponent
    {
        return $this;
    }

    public function preEntityDelete(): AbstractLeafEntityComponent
    {
        return $this;
    }

    public function postEntityCreate(EntityComponentInterface $source_component): AbstractLeafEntityComponent
    {
        return $this;
    }

    public function postEntityLoad(): AbstractLeafEntityComponent
    {
        return $this;
    }

    public function postEntitySave(): AbstractLeafEntityComponent
    {
        return $this;
    }

    public function postEntityDelete(): AbstractLeafEntityComponent
    {
        return $this;
    }

    public function get()
    {
        return null;
    }

    public function set($content): self
    {
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): AbstractLeafEntityComponent
    {
        $this->assertValidId($id);
        $this->id = $id;
        return $this;
    }

    public function getParent(): EntityComponentInterface
    {
        $this->assertValidParent();
        return $this->parent;
    }

    public function setParent(EntityComponentInterface $entity_component): AbstractLeafEntityComponent
    {
        $this->parent = $entity_component;
        return $this;
    }

    public function removeParent(): AbstractLeafEntityComponent
    {
        $this->parent = null;
        return $this;
    }

    public function addChild(string $id, EntityComponentInterface $entity_component): EntityComponentInterface
    {
        throw new Exception('This entity component cannot have children.');
    }

    public function replaceChild(string $id, EntityComponentInterface $entity_component): EntityComponentInterface
    {
        throw new Exception('This entity component cannot have children.');
    }

    public function removeChild(string $id): EntityComponentInterface
    {
        throw new Exception('This entity component cannot have children.');
    }

    public function removeAllChildren(): EntityComponentInterface
    {
        throw new Exception('This entity component cannot have children.');
    }

    public function getChildren(): array
    {
        return [];
    }

    public function getDescendants(): array
    {
        return [];
    }

    public function getChild(string $id): EntityComponentInterface
    {
        throw new Exception('This entity component cannot have children.');
    }

    public function hasChild(string $id): bool
    {
        return false;
    }

    public function getValidationErrors(): array
    {
        return $this->validationErrors;
    }

    public function isInAValidState(): bool
    {
        return empty($this->validationErrors);
    }

    public function isPartOfAStaticHierarchy(): bool
    {
        return $this->partOfAStaticHierarchy;
    }

    public function install(): void
    {
    }

    public function setPartOfAStaticHierarchy(bool $part_of_a_static_hierarchy): AbstractLeafEntityComponent
    {
        $this->partOfAStaticHierarchy = $part_of_a_static_hierarchy;
        return $this;
    }

    public function assertValidId(string $id): void
    {
        if (0 === preg_match('/^[a-z][a-z0-9_]*$/', $id)) {
            throw new Exception('"' . $id . '" is not a valid entity component id. It must only contain lowercase letters, digits or underscores, and begin with a letter.');
        }
    }

    public function assertValidParent(): void
    {
        if (null === $this->parent) {
            throw new Exception('No parent component was set.');
        }
    }

}
