<?php

/*
 * Copyright 2022, 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Doctrine\DBAL\Schema\Schema;
use Exception;
use Psr\Http\Message\UploadedFileInterface;
use Throwable;
use Vitya\CmsComponent\Db\CmsDb;
use Vitya\Component\FileStorage\LocalFileStorageInterface;
use Vitya\CmsComponent\EntityComponent\EntityComponentInterface;
use Vitya\CmsComponent\Image\ResponsiveImageSourceInterface;
use Vitya\CmsComponent\IndexableInterface;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Image\ImageFactoryInterface;
use Vitya\Component\TemporaryStorage\TemporaryStorageInterface;

class FileEntityComponent extends AbstractLeafEntityComponent implements IndexableInterface, ResponsiveImageSourceInterface
{
    private $cmsDb = null;
    private $fileStorage = null;
    private $imageFactory = null;
    private $temporaryStorage = null;
    private $sourceFile = null;
    private $uploadedFile = null;
    protected $content = [];
    private $viewPermissionName = null;
    private $modifyPermissionName = null;
    private $responsiveImageFormatsJsonPath = '';

    public function __construct(
        CmsDb $cms_db,
        LocalFileStorageInterface $file_storage,
        ImageFactoryInterface $image_factory,
        TemporaryStorageInterface $temporary_storage,
        string $view_permission_name = null,
        string $modify_permission_name = null,
        string $responsive_image_formats_json_path = ''
    ) {
        $this->cmsDb = $cms_db;
        $this->fileStorage = $file_storage;
        $this->imageFactory = $image_factory;
        $this->temporaryStorage = $temporary_storage;
        $this->content = [
            'original_filename' => '',
            'size' => 0,
            'mime_type' => 'application/octet-stream',
            'is_an_image' => false,
            'width' => 0,
            'height' => 0,
        ];
        $this->viewPermissionName = $view_permission_name;
        $this->modifyPermissionName = $modify_permission_name;
        $this->responsiveImageFormatsJsonPath = $responsive_image_formats_json_path;
    }

    public function getFileStorage(): LocalFileStorageInterface
    {
        return $this->fileStorage;
    }

    public function getImageFactory(): ImageFactoryInterface
    {
        return $this->imageFactory;
    }

    public function getTemporaryStorage(): TemporaryStorageInterface
    {
        return $this->temporaryStorage;
    }

    public function __isset(string $name): bool
    {
       return in_array($name, [
            'original_filename',
            'size',
            'mime_type',
            'is_an_image',
            'width',
            'height',
       ]);
    }

    public function __get(string $name)
    {
        if ('original_filename' === $name) {
            return $this->get()['original_filename'];
        }
        if ('size' === $name) {
            return $this->get()['size'];
        }
        if ('mime_type' === $name) {
            return $this->get()['mime_type'];
        }
        if ('is_an_image' === $name) {
            return $this->get()['is_an_image'];
        }
        if ('width' === $name) {
            return $this->get()['width'];
        }
        if ('height' === $name) {
            return $this->get()['height'];
        }
        throw new Exception('Cannot find any accessible "' . $name . '" property.');
    }

    public function modifySchema(Schema $schema): Schema
    {
        $table = $schema->createTable($this->getIndexTableName());
        $table->addColumn('entity_id', 'integer', ['unsigned' => true]);
        $table->setPrimaryKey(['entity_id']);
        $table->addIndex(['entity_id']);
        return $schema;
    }

    public function describe()
    {
        return [
            'original_filename' => 'string',
            'size' => 'int',
            'mime_type' => 'string',
            'is_an_image' => 'bool',
            'width' => 'int',
            'height' => 'int',
        ];
    }

    public function index(): void
    {
        $this->deleteFromIndex();
        $conn = $this->cmsDb->getConnection();
        // TODO.
    }

    public function deleteFromIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->delete(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
            ]
        );
    }

    public function purgeIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->executeQuery('DELETE FROM ' . $this->getIndexTableName());
    }

    public function preEntitySave(): self
    {
        $source_file = $this->getSourceFile();
        $uploaded_file = $this->getUploadedFile();
        if (null !== $source_file) {
            $this->assertFileDirExists();
            $this->updateContentMetadata($source_file);
            $this->getFileStorage()->copyLocalFile($source_file, $this->getFilePath());
            $this->setSourceFile(null);
            $this->content['original_filename'] = basename($source_file);
        } elseif (null !== $uploaded_file && \UPLOAD_ERR_OK === $uploaded_file->getError()) {
            $this->assertFileDirExists();
            $tmp_file = $this->getTemporaryStorage()->moveUploadedFile($uploaded_file);
            $this->updateContentMetadata($this->getTemporaryStorage()->getFilePath($tmp_file));
            $this->getFileStorage()->copyLocalFile($this->getTemporaryStorage()->getFilePath($tmp_file), $this->getFilePath());
            $this->setUploadedFile(null);
            $this->content['original_filename'] = $uploaded_file->getClientFilename();
        }
        return $this;
    }

    public function preEntityDelete(): self
    {
        if ($this->getFileStorage()->isFile($this->getFilePath())) {
            $this->getFileStorage()->delete($this->getFilePath());
        }
        $this->resetContentMetadata();
        return $this;
    }

    public function postEntityCreate(EntityComponentInterface $source_component): self
    {
        $this->assertFileDirExists();
        if ($this->getFileStorage()->isFile($source_component->getFilePath())) {
            $this->getFileStorage()->copy($source_component->getFilePath(), $this->getFilePath());
        }
        return $this;
    }

    public function get()
    {
        return $this->content;
    }

    public function set($content): self
    {
        $this->content = [
            'original_filename' => '',
            'size' => 0,
            'mime_type' => 'application/octet-stream',
            'is_an_image' => false,
            'width' => 0,
            'height' => 0,
        ];
        if (false === is_array($content)) {
            throw new Exception('FileEntityComponent::set() requires an array of properties.');
        }
        if (isset($content['original_filename']) && is_string($content['original_filename'])) {
            $this->content['original_filename'] = $content['original_filename'];
        }
        if (isset($content['size']) && is_int($content['size'])) {
            $this->content['size'] = $content['size'];
        }
        if (isset($content['mime_type']) && is_string($content['mime_type'])) {
            $this->content['mime_type'] = $content['mime_type'];
        }
        if (isset($content['is_an_image']) && is_bool($content['is_an_image'])) {
            $this->content['is_an_image'] = $content['is_an_image'];
        }
        if (isset($content['width']) && is_int($content['width'])) {
            $this->content['width'] = $content['width'];
        }
        if (isset($content['height']) && is_int($content['height'])) {
            $this->content['height'] = $content['height'];
        }
        return $this;
    }

    public function getFilePath(): string
    {
        $component_relative_address = (str_replace($this->getEntity()->getAddress() . '/', '', $this->getAddress()));
        $file_name = str_replace('/', '_', $component_relative_address) . '_' . $this->getEntity()->getId();
        return ''
            . '/FileEntityComponent'
            . '/' . $this->getEntity()->getCollectionMachineName()
            . '/' . floor($this->getEntity()->getId() / 100)
            . '/' . $file_name
        ;
    }

    public function getFileDir(): string
    {
        return dirname($this->getFilePath());
    }

    public function getSourceFile(): ?string
    {
        return $this->sourceFile;
    }

    public function setSourceFile(string $source_file = null): self
    {
        $this->sourceFile = $source_file;
        return $this;
    }

    public function getUploadedFile(): ?UploadedFileInterface
    {
        return $this->uploadedFile;
    }

    public function setUploadedFile(UploadedFileInterface $uploaded_file = null): self
    {
        $this->uploadedFile = $uploaded_file;
        return $this;
    }

    public function getViewPermissionName(): ?string
    {
        return $this->viewPermissionName;
    }

    public function setViewPermissionName(string $view_permission_name = null): self
    {
        $this->viewPermissionName = $view_permission_name;
        return $this;
    }

    public function getModifyPermissionName(): ?string
    {
        return $this->modifyPermissionName;
    }

    public function setModifyPermissionName(string $modify_permission_name = null): self
    {
        $this->modifyPermissionName = $modify_permission_name;
        return $this;
    }

    public function getResponsiveImageFormatsJsonPath(): string
    {
        return $this->responsiveImageFormatsJsonPath;
    }

    public function assertFileDirExists()
    {
        $file_dir = $this->getFileDir();
        if (false === $this->getFileStorage()->fileExists($file_dir)) {
            if (false === $this->getFileStorage()->createDir($file_dir)) {
                throw new Exception('Could not create a directory for the file (' . $file_dir . ').');
            }
        }
        if (false === $this->getFileStorage()->isDir($file_dir)) {
            throw new Exception('File path exists but is not a directory (' . $file_dir . ').');
        }
    }

    public function resetContentMetadata(): self
    {
        $this->content['size'] = 0;
        $this->content['mime_type'] = 'application/octet-stream';
        $this->content['is_an_image'] = false;
        $this->content['width'] = 0;
        $this->content['height'] = 0;
        return $this;
    }

    public function updateContentMetadata(string $local_file_path): self
    {
        $this->resetContentMetadata();
        if (is_file($local_file_path)) {
            $this->content['size'] = (int) filesize($local_file_path);
            try {
                $potential_image = $this->getImageFactory()->makeImage()->loadFromLocalFile($local_file_path);
                $this->content['is_an_image'] = true;
                $this->content['width'] = $potential_image->getWidth();
                $this->content['height'] = $potential_image->getHeight();
            } catch (Throwable $t) {
                // Nothing to do, we just meant to determine whether it was a valid image.
            }
            $this->content['mime_type'] = mime_content_type($local_file_path);
        }
        return $this;
    }

    public function getIndexTableName(): string
    {
        $component_address = $this->getAddress();
        $entity_address = $this->getEntity()->getAddress();
        $relative_address = str_replace(' ' . $entity_address, '', ' ' . $component_address);
        $slug = str_replace('-', '_', str_replace('/', '_', $relative_address));
        return $this->cmsDb->getPrefix() . '_index_' . str_replace('-', '_', $this->getEntity()->getMachineName()) . $slug;
    }

    public function canBeViewed(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->viewPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->viewPermissionName, $user);
        }
        return true;
    }

    public function canBeModified(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->modifyPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->modifyPermissionName, $user);
        }
        return true;
    }

    public function getFormattedSize(): string
    {
        $bytes = $this->content['size'];
        $units = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $factor = floor((strlen((string) $bytes) - 1) / 3);
        if (isset($units[$factor])) {
            return sprintf('%.1f', $bytes / pow(1000, $factor)) . ' ' . $units[$factor];
        }
        return '?';
    }

    public function parseResponsiveImageFormatsJsonFile(string $json_path): array
    {
        $config = json_decode(file_get_contents($json_path), true, 16, JSON_THROW_ON_ERROR);
        if (false === is_array($config)) {
            return [];
        }
        $formats = [];
        foreach ($config as $format_name => $format_description) {
            $format_name = (string) $format_name;
            $format = [
                'transformation' => '',
                'mime_type' => 'image/jpeg',
                'output_options' => [],
                'max_age' => 0,
            ];
            if (isset($format_description['transformation']) && is_string($format_description['transformation'])) {
                $format['transformation'] = $format_description['transformation'];
            }
            if (isset($format_description['mime_type']) && is_string($format_description['mime_type'])) {
                $format['mime_type'] = $format_description['mime_type'];
            }
            if (isset($format_description['output_options']) && is_array($format_description['output_options'])) {
                $format['output_options'] = $format_description['output_options'];
            }
            if (isset($format_description['max_age']) && is_int($format_description['max_age'])) {
                $format['max_age'] = $format_description['max_age'];
            }
            $formats[$format_name] = $format;
        }
        return $formats;
    }

    // ResponsiveImageSourceInterface implementation
    // -------------------------------------------------------------------------

    public function getResponsiveImageSourceFilePath(): string
    {
        return $this->getFileStorage()->getLocalFilesystemPath($this->getFilePath());
    }

    public function getResponsiveImageSourceWidth(): int
    {
        return (int) $this->content['width'];
    }

    public function getResponsiveImageSourceHeight(): int
    {
        return (int) $this->content['height'];
    }

    public function getResponsiveImageSourceAutocrop(): bool
    {
        return false;
    }

    public function getResponsiveImageSourceCoiX(): float
    {
        return 0.5;
    }

    public function getResponsiveImageSourceCoiY(): float
    {
        return 0.5;
    }

    public function responsiveImageSourceIsPublic(): bool
    {
        return true;
    }

    public function getAvailableOutputFormats(): array
    {
        static $formats = null;
        if (null === $formats) {
            $formats = $this->parseResponsiveImageFormatsJsonFile($this->getResponsiveImageFormatsJsonPath());
        }
        return $formats;
    }

}
