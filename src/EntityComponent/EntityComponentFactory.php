<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Exception;
use Vitya\CmsComponent\Datum\DatumInterface;
use Vitya\Component\L10n\LocaleCatalog;
use Vitya\Component\Service\DependencyInjectorInterface;

class EntityComponentFactory
{
    private $dependencyInjector = null;
    private $localeCatalog = null;

    public function __construct(DependencyInjectorInterface $dependency_injector, LocaleCatalog $locale_catalog)
    {
        $this->dependencyInjector = $dependency_injector;
        $this->localeCatalog = $locale_catalog;
    }

    public function getDependencyInjector(): DependencyInjectorInterface
    {
        return $this->dependencyInjector;
    }

    public function getLocaleCatalog(): LocaleCatalog
    {
        return $this->localeCatalog;
    }

    public function make(string $class_name)
    {
        $entity_component = $this->getDependencyInjector()->make($class_name);
        if (false === $entity_component instanceof EntityComponentInterface) {
            throw new Exception(get_class($entity_component) . ' does not implement EntityComponentInterface.');
        }
        return $entity_component;
    }

    public function createComponentsFromDescription(EntityComponentInterface $root, array $description)
    {
        foreach ($description as $id => $datum_description) {
            if (false === isset($datum_description['class']) || false === is_string($datum_description['class'])) {
                throw new Exception('A datum class name must be provided (element "' . $id . '" of the description).');
            }
            if (false === $datum_description['options']) {
                $datum_description['options'] = [];
            }
            if (false === isset($datum_description['localized'])) {
                $datum_description['localized'] = false;
            } else {
                $datum_description['localized'] = (bool) $datum_description['localized'];
            }
            if ($datum_description['localized']) {
                $l10n_component = new L10nEntityComponent($this->getLocaleCatalog());
                $root->addChild($id, $l10n_component);
                foreach ($this->getLocaleCatalog()->getLocales() as $locale) {
                    $datum = $this->make($datum_description['class']);
                    if (false === $datum instanceof DatumInterface) {
                        throw new Exception(get_class($datum) . ' does not implement DatumInterface.');
                    }
                    $l10n_component->addChild($locale->getId(), $datum);
                    $datum->setOptions($datum_description['options']);
                }
            } else {
                $datum = $this->make($datum_description['class']);
                if (false === $datum instanceof DatumInterface) {
                    throw new Exception(get_class($datum) . ' does not implement DatumInterface.');
                }
                $root->addChild($id, $datum);
                $datum->setOptions($datum_description['options']);
            }
        }
    }

}
