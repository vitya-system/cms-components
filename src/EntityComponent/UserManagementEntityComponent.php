<?php

/*
 * Copyright 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Exception;
use Vitya\CmsComponent\Db\CmsDb;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\Authentication\UserInterface;

class UserManagementEntityComponent extends AbstractLeafEntityComponent
{
    private $authn = null;
    private $cmsDb = null;
    private $omnipotent = false;
    private $authenticationRealm = '';
    private $groups = [];
    private $viewPermissionName = null;
    private $modifyPermissionName = null;

    public function __construct(
        AuthenticationService $authn,
        CmsDb $cms_db,
        string $authentication_realm,
        string $view_permission_name = null,
        string $modify_permission_name = null
    ) {
        $this->authn = $authn;
        $this->cmsDb = $cms_db;
        $this->authenticationRealm = $authentication_realm;
        $this->viewPermissionName = $view_permission_name;
        $this->modifyPermissionName = $modify_permission_name;
    }

    public function __isset(string $name): bool
    {
       return in_array($name, [
            'omnipotent',
            'groups',
       ]);
    }

    public function __get(string $name)
    {
        if ('omnipotent' === $name) {
            return $this->get()['omnipotent'];
        }
        if ('groups' === $name) {
            return $this->get()['groups'];
        }
        throw new Exception('Cannot find any accessible "' . $name . '" property.');
    }

    public function describe()
    {
        return [
            'omnipotent' => 'bool',
            'groups' => 'array',
        ];
    }

    public function get()
    {
        return [
            'omnipotent' => $this->omnipotent,
            'groups' => $this->groups,
        ];
    }

    public function set($content): self
    {
        $this->setOmnipotent(false);
        $this->setGroups([]);
        if (false === is_array($content)) {
            throw new Exception('UserManagementEntityComponent::set() requires an array of properties.');
        }
        if (isset($content['omnipotent']) && is_bool($content['omnipotent'])) {
            $this->setOmnipotent($content['omnipotent']);
        }
        if (isset($content['groups']) && is_array($content['groups'])) {
            $this->setGroups($content['groups']);
        }
        return $this;
    }

    public function getAuthenticationService(): AuthenticationService
    {
        return $this->authn;
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function getAuthenticationRealm(): string
    {
        return $this->authenticationRealm;
    }

    public function isOmnipotent(): bool
    {
        return $this->omnipotent;
    }

    public function setOmnipotent(bool $omnipotent): UserManagementEntityComponent
    {
        $this->omnipotent = $omnipotent;
        return $this;
    }

    public function getGroups(): array
    {
        return $this->groups;
    }

    public function setGroups(array $groups): UserManagementEntityComponent
    {
        $sanitized_groups = [];
        foreach ($groups as $group) {
            if (is_int($group) && false === in_array($group, $sanitized_groups)) {
                $sanitized_groups[] = $group;
            }
        }
        $this->groups = $sanitized_groups;
        return $this;
    }

    public function getViewPermissionName(): ?string
    {
        return $this->viewPermissionName;
    }

    public function setViewPermissionName(string $view_permission_name = null): UserManagementEntityComponent
    {
        $this->viewPermissionName = $view_permission_name;
        return $this;
    }

    public function getModifyPermissionName(): ?string
    {
        return $this->modifyPermissionName;
    }

    public function setModifyPermissionName(string $modify_permission_name = null): UserManagementEntityComponent
    {
        $this->modifyPermissionName = $modify_permission_name;
        return $this;
    }

    public function canBeViewed(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->viewPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->viewPermissionName, $user);
        }
        return true;
    }

    public function canBeModified(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->modifyPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->modifyPermissionName, $user);
        }
        return true;
    }

    public function canBeMadeOmnipotent(UserInterface $user = null): bool
    {
        if (null === $user) {
            return false;
        }
        if (get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
            return false;
        }
        return $user->isOmnipotent();
    }

}
