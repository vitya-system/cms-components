<?php

/*
 * Copyright 2022, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Doctrine\DBAL\Schema\Schema;
use Exception;
use Vitya\CmsComponent\Db\CmsDb;
use Vitya\CmsComponent\IndexableInterface;
use Vitya\Component\Acl\Acl;
use Vitya\Component\Acl\AclInterface;
use Vitya\Component\Authentication\AuthenticationService;
use Vitya\Component\Authentication\UserInterface;

class AclEntityComponent extends AbstractLeafEntityComponent implements IndexableInterface
{
    private $authn = null;
    private $cmsDb = null;
    private $acl = null;
    private $possiblePermissions = [];
    private $viewPermissionName = null;
    private $modifyPermissionName = null;
    private $authenticationRealm = '';

    public function __construct(
        AuthenticationService $authn,
        CmsDb $cms_db,
        string $authentication_realm,
        string $view_permission_name = null,
        string $modify_permission_name = null
    ) {
        $this->authn = $authn;
        $this->cmsDb = $cms_db;
        $this->acl = new Acl();
        $this->authenticationRealm = $authentication_realm;
        $this->viewPermissionName = $view_permission_name;
        $this->modifyPermissionName = $modify_permission_name;
    }

    public function __isset(string $name): bool
    {
        if ('permissions' === $name) {
            return true;
        }
        return false;
    }

    public function __get(string $name)
    {
        if ('permissions' === $name) {
            return $this->acl->getPermissions();
        }
        throw new Exception('Cannot find any accessible "' . $name . '" property.');
    }

    public function modifySchema(Schema $schema): Schema
    {
        $table = $schema->createTable($this->getIndexTableName());
        $table->addColumn('entity_id', 'integer', ['unsigned' => true]);
        $table->addColumn('type', 'string', ['length' => 1]);
        $table->addColumn('identifier', 'integer', ['unsigned' => true]);
        $table->addColumn('permission', 'string', ['length' => 1]);
        $table->addIndex(['type', 'identifier', 'permission']);
        $table->addIndex(['entity_id']);
        return $schema;
    }

    public function describe()
    {
        return ['permissions' => 'array'];
    }

    public function duplicate(): self
    {
        $new_entity_component = clone $this;
        $new_entity_component->acl = clone $this->acl;
        return $new_entity_component;
    }

    public function index(): void
    {
        $this->deleteFromIndex();
        $conn = $this->cmsDb->getConnection();
        foreach ($this->acl->getPermissions() as $permission) {
            $conn->insert(
                $this->getIndexTableName(),
                [
                    'entity_id' => $this->getEntity()->getId(),
                    'type' => $permission['type'],
                    'identifier' => $permission['identifier'],
                    'permission' => $permission['permission'],
                ]
            );
        }
    }

    public function deleteFromIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->delete(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
            ]
        );
    }

    public function purgeIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->executeQuery('DELETE FROM ' . $this->getIndexTableName());
    }

    public function get()
    {
        return $this->acl->getPermissions();
    }

    public function set($content): self
    {
        $this->acl->resetPermissions();
        if (false === is_array($content)) {
            throw new Exception('AclEntityComponent::set() requires an array of permissions.');
        }
        $possible_permissions = $this->getPossiblePermissions();
        foreach ($content as $permission) {
            if (false === is_array($permission)) {
                throw new Exception('Permission must be described as an array.');
            }
            if (false === in_array($permission['permission'], $possible_permissions)) {
                continue;
            }
            $this->acl->addPermission($permission['type'], $permission['identifier'], $permission['permission']); 
        }
        return $this;
    }

    public function getComponentAcl(): AclInterface
    {
        return $this->acl;
    }

    public function getAuthenticationService(): AuthenticationService
    {
        return $this->authn;
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function getAuthenticationRealm(): string
    {
        return $this->authenticationRealm;
    }

    public function getPossiblePermissions(): array
    {
        return $this->possiblePermissions;
    }

    public function setPossiblePermissions(array $possible_permissions): AclEntityComponent
    {
        $this->possiblePermissions = $possible_permissions;
        return $this;
    }

    public function getViewPermissionName(): ?string
    {
        return $this->viewPermissionName;
    }

    public function setViewPermissionName(string $view_permission_name = null): AclEntityComponent
    {
        $this->viewPermissionName = $view_permission_name;
        return $this;
    }

    public function getModifyPermissionName(): ?string
    {
        return $this->modifyPermissionName;
    }

    public function setModifyPermissionName(string $modify_permission_name = null): AclEntityComponent
    {
        $this->modifyPermissionName = $modify_permission_name;
        return $this;
    }

    public function getIndexTableName(): string
    {
        $component_address = $this->getAddress();
        $entity_address = $this->getEntity()->getAddress();
        $relative_address = str_replace(' ' . $entity_address, '', ' ' . $component_address);
        $slug = str_replace('-', '_', str_replace('/', '_', $relative_address));
        return $this->cmsDb->getPrefix() . '_index_' . str_replace('-', '_', $this->getEntity()->getMachineName()) . $slug;
    }

    public function canBeViewed(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->viewPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->viewPermissionName, $user);
        }
        return true;
    }

    public function canBeModified(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->modifyPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->modifyPermissionName, $user);
        }
        return true;
    }

}
