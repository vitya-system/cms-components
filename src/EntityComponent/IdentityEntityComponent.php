<?php

/*
 * Copyright 2022, 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Doctrine\DBAL\Schema\Schema;
use Exception;
use Vitya\CmsComponent\Db\CmsDb;
use Vitya\CmsComponent\IndexableInterface;
use Vitya\Component\Authentication\UserInterface;

class IdentityEntityComponent extends AbstractLeafEntityComponent implements IndexableInterface
{
    const VALIDATION_ERROR_EMPTY_USERNAME = 1;
    const VALIDATION_ERROR_EMPTY_PASSWORD = 2;
    const VALIDATION_ERROR_PASSWORD_TOO_SHORT = 3;

    private $cmsDb = null;
    private $username = '';
    private $password = '';
    private $clearPassword = null;
    private $viewPermissionName = null;
    private $modifyPermissionName = null;

    public function __construct(CmsDb $cms_db, string $view_permission_name = null, string $modify_permission_name = null)
    {
        $this->cmsDb = $cms_db;
        $this->viewPermissionName = $view_permission_name;
        $this->modifyPermissionName = $modify_permission_name;
        $this->updateValidationState();
    }

    public function __isset(string $name): bool
    {
       return in_array($name, [
            'username',
            'password',
       ]);
    }

    public function __get(string $name)
    {
        if ('username' === $name) {
            return $this->get()['username'];
        }
        if ('password' === $name) {
            return $this->get()['password'];
        }
        throw new Exception('Cannot find any accessible "' . $name . '" property.');
    }

    public function modifySchema(Schema $schema): Schema
    {
        $table = $schema->createTable($this->getIndexTableName());
        $table->addColumn('entity_id', 'integer', ['unsigned' => true]);
        $table->addColumn('username', 'string');
        $table->addUniqueIndex(['entity_id']);
        $table->addUniqueIndex(['username']);
        return $schema;
    }

    public function describe()
    {
        return [
            'username' => 'string',
            'password' => 'string',
        ];
    }

    public function index(): void
    {
        $this->deleteFromIndex();
        if ('' === $this->username) {
            return;
        }
        $conn = $this->cmsDb->getConnection();
        $r = $conn->insert(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
                'username' => $this->username,
            ]
        );
    }

    public function deleteFromIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->delete(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
            ]
        );
    }

    public function purgeIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->executeQuery('DELETE FROM ' . $this->getIndexTableName());
    }

    public function get()
    {
        return [
            'username' => $this->username,
            'password' => $this->password,
        ];
    }

    public function set($content): self
    {
        $this->username = '';
        $this->password = '';
        if (false === is_array($content)) {
            throw new Exception('IdentityEntityComponent::set() requires an array of properties.');
        }
        if (isset($content['username']) && is_string($content['username'])) {
            $this->username = trim($content['username']);
        }
        if (isset($content['password']) && is_string($content['password'])) {
            $this->password = $content['password'];
        }
        $this->updateValidationState();
        return $this;
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function getClearPassword(): ?string
    {
        return $this->clearPassword;
    }

    public function setClearPassword(string $clear_password = null): IdentityEntityComponent
    {
        $this->clearPassword = $clear_password;
        if (null !== $clear_password) {
            $this->password = $this->hashPassword($clear_password);
        } else {
            $this->password = '';
        }
        $this->updateValidationState();
        return $this;
    }

    public function getViewPermissionName(): ?string
    {
        return $this->viewPermissionName;
    }

    public function setViewPermissionName(string $view_permission_name = null): IdentityEntityComponent
    {
        $this->viewPermissionName = $view_permission_name;
        return $this;
    }

    public function getModifyPermissionName(): ?string
    {
        return $this->modifyPermissionName;
    }

    public function setModifyPermissionName(string $modify_permission_name = null): IdentityEntityComponent
    {
        $this->modifyPermissionName = $modify_permission_name;
        return $this;
    }

    public function getIndexTableName(): string
    {
        $component_address = $this->getAddress();
        $entity_address = $this->getEntity()->getAddress();
        $relative_address = str_replace(' ' . $entity_address, '', ' ' . $component_address);
        $slug = str_replace('-', '_', str_replace('/', '_', $relative_address));
        return $this->cmsDb->getPrefix() . '_index_' . str_replace('-', '_', $this->getEntity()->getMachineName()) . $slug;
    }

    public function canBeViewed(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->viewPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->viewPermissionName, $user);
        }
        return true;
    }

    public function canBeModified(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->modifyPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->modifyPermissionName, $user);
        }
        return true;
    }

    public function canHavePasswordModified(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->modifyPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            if ($user->getUserIdentifier() === $this->getEntity()->getUserIdentifier()) {
                return true;
            }
            return $entity_acl->isGranted($this->modifyPermissionName, $user);
        }
        return true;
    }

    public function updateValidationState(): void
    {
        $this->validationErrors = [];
        if ('' === $this->username) {
            $this->validationErrors[] = self::VALIDATION_ERROR_EMPTY_USERNAME;
        }
        if (null !== $this->clearPassword) {
            if ('' === $this->clearPassword) {
                $this->validationErrors[] = self::VALIDATION_ERROR_EMPTY_PASSWORD;
            } elseif (mb_strlen($this->clearPassword) < 8) {
                $this->validationErrors[] = self::VALIDATION_ERROR_PASSWORD_TOO_SHORT;
            } 
        }
    }

    public function checkPassword(string $clear_password): bool
    {
        return password_verify($clear_password, $this->password);
    }

    public function hashPassword(string $clear_password): string
    {
        return password_hash($clear_password, PASSWORD_DEFAULT);
    }

    public function usernameIsAlreadyInUse(): bool
    {
        $conn = $this->getCmsDb()->getConnection();
        $query_builder = $conn->createQueryBuilder();
        $query_builder
            ->select('entity_id')
            ->from($this->getIndexTableName())
            ->where('username = :username')
            ->setParameter('username', $this->getUsername())
        ;
        $result = $query_builder->executeQuery();
        if (false !== $row = $result->fetchAssociative()) {
            if ($this->getEntity()->getId() !== (int) $row['entity_id']) {
                return true;
            }
        }
        return false;
    }

}
