<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\EntityComponent;

use Doctrine\DBAL\Schema\Schema;
use Exception;
use Vitya\CmsComponent\Db\CmsDb;
use Vitya\CmsComponent\IndexableInterface;
use Vitya\Component\Authentication\UserInterface;

class PublicationEntityComponent extends AbstractLeafEntityComponent implements IndexableInterface
{
    const VALIDATION_ERROR_UNINITIALIZED = 1;
    const VALIDATION_ERROR_INVALID_END_UTS = 2;
    
    private $cmsDb = null;
    private $publishable = false;
    private $startUts = null;
    private $endUts = null;
    private $viewPermissionName = null;
    private $modifyPermissionName = null;

    public function __construct(
        CmsDb $cms_db,
        string $view_permission_name = null,
        string $modify_permission_name = null
    ) {
        $this->cmsDb = $cms_db;
        $this->viewPermissionName = $view_permission_name;
        $this->modifyPermissionName = $modify_permission_name;
        $this->validationErrors = [self::VALIDATION_ERROR_UNINITIALIZED];
        $this->updateValidationState();
    }

    public function __isset(string $name): bool
    {
        if (in_array($name, ['publishable', 'start_uts', 'end_uts', 'published'])) {
            return true;
        }
        return false;
    }

    public function __get(string $name)
    {
        if ('publishable' === $name) {
            return $this->isPublishable();
        }
        if ('start_uts' === $name) {
            return $this->getStartUts();
        }
        if ('end_uts' === $name) {
            return $this->getEndUts();
        }
        if ('published' === $name) {
            return $this->isPublished();
        }
        throw new Exception('Cannot find any accessible "' . $name . '" property.');
    }

    public function modifySchema(Schema $schema): Schema
    {
        $table = $schema->createTable($this->getIndexTableName());
        $table->addColumn('entity_id', 'integer', ['unsigned' => true]);
        $table->addColumn('publishable', 'boolean', []);
        $table->addColumn('start_uts', 'bigint', ['notnull' => true]);
        $table->addColumn('end_uts', 'bigint', ['notnull' => true]);
        $table->addUniqueIndex(['entity_id']);
        $table->addIndex(['publishable']);
        $table->addIndex(['start_uts']);
        $table->addIndex(['end_uts']);
        return $schema;
    }

    public function describe()
    {
        return [
            'publishable' => 'bool',
            'start_uts' => 'int',
            'end_uts' => 'int',
            'published' => 'bool',
        ];
    }

    public function duplicate(): self
    {
        $new_entity_component = clone $this;
        return $new_entity_component;
    }

    public function index(): void
    {
        $this->deleteFromIndex();
        $conn = $this->cmsDb->getConnection();
        // Min and max 64-bit integer values are used when no explicit
        // publication time was set. 
        $conn->insert(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
                'publishable' => $this->isPublishable(),
                'start_uts' => (null === $this->getStartUts() ? -9223372036854775807 : $this->getStartUts()),
                'end_uts' =>  (null === $this->getEndUts() ? 9223372036854775807 : $this->getEndUts()),
            ]
        );
    }

    public function deleteFromIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->delete(
            $this->getIndexTableName(),
            [
                'entity_id' => $this->getEntity()->getId(),
            ]
        );
    }

    public function purgeIndex(): void
    {
        $conn = $this->cmsDb->getConnection();
        $conn->executeQuery('DELETE FROM ' . $this->getIndexTableName());
    }

    public function get()
    {
        return [
            'publishable' => $this->isPublishable(),
            'start_uts' => $this->getStartUts(),
            'end_uts' => $this->getEndUts(),
        ];
    }

    public function set($content): self
    {
        $this->reset();
        if (is_array($content)) {
            if (isset($content['publishable']) && is_bool(($content['publishable']))) {
                $this->publishable = $content['publishable'];
            }
            if ($this->publishable) {
                if (isset($content['start_uts']) && is_int(($content['start_uts']))) {
                    $this->startUts = $content['start_uts'];
                }
                if (isset($content['end_uts']) && is_int(($content['end_uts']))) {
                    $this->endUts = $content['end_uts'];
                }
            }
        }
        $this->updateValidationState();
        return $this;
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function getViewPermissionName(): ?string
    {
        return $this->viewPermissionName;
    }

    public function setViewPermissionName(string $view_permission_name = null): PublicationEntityComponent
    {
        $this->viewPermissionName = $view_permission_name;
        return $this;
    }

    public function getModifyPermissionName(): ?string
    {
        return $this->modifyPermissionName;
    }

    public function setModifyPermissionName(string $modify_permission_name = null): PublicationEntityComponent
    {
        $this->modifyPermissionName = $modify_permission_name;
        return $this;
    }

    public function updateValidationState(): void
    {
        $this->validationErrors = [];
        if (null !== $this->startUts && null !== $this->endUts) {
            if ($this->endUts <= $this->startUts) {
                $this->validationErrors[] = self::VALIDATION_ERROR_INVALID_END_UTS;
            }
        }
    }

    public function getIndexTableName(): string
    {
        $component_address = $this->getAddress();
        $entity_address = $this->getEntity()->getAddress();
        $relative_address = str_replace(' ' . $entity_address, '', ' ' . $component_address);
        $slug = str_replace('-', '_', str_replace('/', '_', $relative_address));
        return $this->cmsDb->getPrefix() . '_index_' . str_replace('-', '_', $this->getEntity()->getMachineName()) . $slug;
    }

    public function canBeViewed(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->viewPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->viewPermissionName, $user);
        }
        return true;
    }

    public function canBeModified(UserInterface $user = null): bool
    {
        if ((null !== $entity_acl = $this->getEntity()->getCmsAcl()) && (null !== $this->modifyPermissionName)) {
            if (null !== $user && get_class($user) !== $this->getEntity()->getCmsUserProvider()->getSupportedUserClass()) {
                return false;
            }
            return $entity_acl->isGranted($this->modifyPermissionName, $user);
        }
        return true;
    }

    public function isPublished(int $now = null): bool
    {
        if (false === $this->publishable) {
            return false;
        }
        if (null === $now) {
            $now = time();
        }
        if (null !== $this->startUts && $now < $this->startUts) {
            return false;
        }
        if (null !== $this->endUts && $now >= $this->endUts) {
            return false;
        }
        return true;
    }

    public function isPublishable(): bool
    {
        return $this->publishable;
    }

    public function getStartUts(): ?int
    {
        return $this->startUts;
    }

    public function getEndUts(): ?int
    {
        return $this->endUts;
    }

    public function reset(): PublicationEntityComponent
    {
        $this->publishable = false;
        $this->startUts = null;
        $this->endUts = null;
        return $this;
    }

}
