<?php

/*
 * Copyright 2024 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Composite;

use Vitya\CmsComponent\EntityComponent\AbstractEntityComponent;
use Vitya\CmsComponent\EntityComponent\EntityComponentFactory;

abstract class AbstractComposite extends AbstractEntityComponent implements CompositeInterface
{
    private $entityComponentFactory = null;

    public function __construct(EntityComponentFactory $entity_component_factory)
    {
        $this->entityComponentFactory = $entity_component_factory;
        $this->getEntityComponentFactory()->createComponentsFromDescription($this, $this->getDataDescription());
    }

    public function __isset(string $name): bool
    {
        if (in_array($name, [
            'class_name',
            'id',
        ])) {
            return true;
        }
        return parent::__isset($name);
    }

    public function __get(string $name)
    {
        if ('class_name' === $name) {
            return get_class($this);
        }
        if ('id' === $name) {
            return $this->getId();
        }
        return parent::__get($name);
    }

    public function getEntityComponentFactory(): EntityComponentFactory
    {
        return $this->entityComponentFactory;
    }

    public function getDataDescription(): array
    {
        $description = [];
        return $description;
    }

}
