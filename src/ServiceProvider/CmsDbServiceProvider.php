<?php

/*
 * Copyright 2021 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\ServiceProvider;

use Vitya\CmsComponent\Db\CmsDb;
use Vitya\Component\Service\ServiceContainer;
use Vitya\Component\Service\ServiceProviderInterface;

class CmsDbServiceProvider implements ServiceProviderInterface
{
    public function getName(): string
    {
        return 'cms_db';
    }

    public function getTypeHints(): array
    {
        return ['Vitya\CmsComponent\Db\CmsDb'];
    }

    public function instantiateService(ServiceContainer $service_container): object
    {
        return new CmsDb(
            $service_container['cms_db__prefix'],
            $service_container->get('db')->getConnection('cms')
        );
    }

}
