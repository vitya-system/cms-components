<?php

/*
 * Copyright 2020 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Controller;

use Vitya\Application\Controller\AbstractController as Application_AbstractController;
use Vitya\CmsComponent\AddressableObject\AddressableObjectStoreInterface;
use Vitya\CmsComponent\Entity\EntityFactoryInterface;

abstract class AbstractController extends Application_AbstractController
{
    // Entity factory related method.

    protected function ao(): AddressableObjectStoreInterface
    {
        $addressable_object_store = $this->getServiceFromHint('Vitya\CmsComponent\AddressableObject\AddressableObjectStoreInterface');
        return $addressable_object_store;
    }

    // Entity factory related method.

    protected function ef(): EntityFactoryInterface
    {
        $entity_factory = $this->getServiceFromHint('Vitya\CmsComponent\Entity\EntityFactoryInterface');
        return $entity_factory;
    }

}
