<?php

/*
 * Copyright 2025 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Controller;

use Exception;
use Psr\Http\Message\ResponseFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Vitya\CmsComponent\Image\ResponsiveImageHelper;
use Vitya\CmsComponent\Image\ResponsiveImageSourceInterface;
use Vitya\Component\Image\ImageFactoryInterface;

class ImageController extends AbstractController
{
    public function image(
        ImageFactoryInterface $image_factory,
        ResponseFactoryInterface $response_factory,
        string $validator,
        string $output_format,
        string $component_address,
        string $filename_extension
    ): ResponseInterface {
        // Can we load a suitable entity component?
        $component = $this->ao()->getFromAddress($component_address);
        if (false === $component instanceof ResponsiveImageSourceInterface) {
            $this->notFound('Cannot load component at address ' . $component_address . '.');
        }
        if (false === $component->responsiveImageSourceIsPublic()) {
            $this->notFound('This component is not public: ' . $component_address . '.');
        }
        $entity = $component->getEntity();
        // Check whether if the validator string has changed.
        $expected_validator = (string) $entity->getLastModificationUts();
        if ($expected_validator !== $validator) {
            $this->permanentRedirect($this->uri(
                $this->getAppParam('default_image_controller_route_name'), [
                    'validator' => $expected_validator,
                    'output_format' => $output_format,
                    'component_address' => $component_address,
                    'filename_extension' => $filename_extension,
                ]
            ));
        }
        // Prepare image treatment.
        $helper = new ResponsiveImageHelper($component, $image_factory);
        $available_output_formats = $component->getAvailableOutputFormats();
        if (false === isset($available_output_formats[$output_format])) {
            $this->notFound('Unknown output format: "' . $output_format . '".');
        }
        $output_format_description = $available_output_formats[$output_format];
        // Check whether filename extension matches.
        $output_file_format = $helper->getOutputFileFormat($component->mime_type, $output_format_description['mime_type']);
        $expected_filename_extension = $output_file_format['filename_extension'];
        if ($expected_filename_extension !== $filename_extension) {
            $this->permanentRedirect($this->uri(
                $this->getAppParam('default_image_controller_route_name'), [
                    'validator' => $expected_validator,
                    'output_format' => $output_format,
                    'component_address' => $component_address,
                    'filename_extension' => $expected_filename_extension,
                ]
            ));
        }
        $output_mime_type = $output_file_format['mime_type'];
        // Create the response.
        $response = $response_factory->createResponse(200);
        $max_age = (int) $output_format_description['max_age'];
        if (0 < $max_age) {
            $response = $response->withHeader('Cache-Control', 'public, immutable, max-age=' . $max_age);
        }
        if (false === $this->isModified($this->getRequest(), $response)) {
            return $this->makeNotModifiedResponse($response);
        }
        $image = $helper->makeImage($available_output_formats[$output_format]['transformation']);
        $response = $response->withHeader('Content-Type', $output_mime_type)->withBody($image->output($output_mime_type, $output_format_description['output_options']));
        return $response;
    }

}
