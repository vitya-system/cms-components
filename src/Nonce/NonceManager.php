<?php

/*
 * Copyright 2022 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Nonce;

use Exception;
use Vitya\CmsComponent\Db\CmsDb;

class NonceManager implements NonceManagerInterface
{
    private $cmsDb = null;

    public function __construct(CmsDb $cms_db) {
        $this->cmsDb = $cms_db;
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function createNonce(string $action, int $duration): string
    {
        $expiration_uts = time() + $duration;
        $token = sha1(''
            . $action
            . $expiration_uts
            . rand()
            . rand()
        );

        $conn = $this->getCmsDb()->getConnection();
        $conn->insert(
            $this->getCmsDb()->getPrefix() . '_nonce',
            [
                'token' => $token,
                'action' => $action,
                'expiration_uts' => $expiration_uts,
            ]
        );
        return $token;
    }

    public function assertValidNonce(string $action, string $token): void
    {
        if ('' === $token) {
            throw new Exception('Nonce required.');
        }
        $table_name = $this->getCmsDb()->getPrefix() . '_nonce';
        $conn = $this->getCmsDb()->getConnection();
        $conn->executeQuery("DELETE FROM " . $table_name . " WHERE expiration_uts < " . time());
        $query_builder = $conn->createQueryBuilder();
        $query_builder
            ->select('action')
            ->from($table_name)
            ->where('token = :token')
            ->setParameter('token', $token)
        ;
        $result = $query_builder->executeQuery();
        if (false === $row = $result->fetchAssociative()) {
            throw new Exception('Invalid nonce.');
        }
        $conn->delete(
            $table_name,
            [
                'token' => $token,
            ]
        );
    }

}
