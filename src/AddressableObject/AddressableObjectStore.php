<?php

/*
 * Copyright 2022 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\AddressableObject;

class AddressableObjectStore implements AddressableObjectInterface, AddressableObjectStoreInterface
{
    private $topLevelProviders = [];

    public function getAddress(): string
    {
        return '/';
    }

    public function getFromRelativeAddress(string $relative_address): ?AddressableObjectInterface
    {
        return $this->getFromAddress('/' . $relative_address);
    }

    public function getFromAddress(string $address): ?AddressableObjectInterface
    {
        if ($address === '/') {
            return $this;
        }
        $relative_address = preg_replace('@^/@', '', $address);
        $relative_address_elements = explode('/', $relative_address);
        $top_level_address = '/' . $relative_address_elements[0];
        if (false === isset($this->topLevelProviders[$top_level_address])) {
            return null;
        }
        array_shift($relative_address_elements);
        return $this->topLevelProviders[$top_level_address]->getFromRelativeAddress(implode('/', $relative_address_elements));
    }

    public function registerTopLevelProvider(AddressableObjectInterface $addressable_object): void
    {
        $this->topLevelProviders[$addressable_object->getAddress()] = $addressable_object;
    }

}
