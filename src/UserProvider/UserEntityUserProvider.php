<?php

/*
 * Copyright 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\UserProvider;

use Exception;
use Vitya\CmsComponent\AddressableObject\AddressableObjectStoreInterface;
use Vitya\CmsComponent\Db\CmsDb;
use Vitya\CmsComponent\Entity\AbstractUserEntity;
use Vitya\CmsComponent\Entity\EntityFactoryInterface;
use Vitya\Component\Authentication\UserInterface;
use Vitya\Component\Authentication\UserProviderInterface;
use Vitya\Component\Authentication\UserWithUsernameProviderInterface;

class UserEntityUserProvider implements UserProviderInterface, UserWithUsernameProviderInterface
{
    public $cmsDb = null;
    public $entityFactory = null;
    public $supportedUserClass = '';
    public $userGroups = [];

    public function __construct(
        CmsDb $cms_db,
        EntityFactoryInterface $entity_factory,
        string $supported_user_class,
        array $user_groups
    ) {
        $this->cmsDb = $cms_db;
        $this->entityFactory = $entity_factory;
        $this->supportedUserClass = $supported_user_class;
        foreach ($user_groups as $user_group_identifier => $user_group_name) {
            $this->userGroups[(int) $user_group_identifier] = (string) $user_group_name;
        }
    }

    public function loadUserByIdentifier(int $identifier): ?UserInterface
    {
        $user = $this->entityFactory->load($this->getSupportedUserClass(), $identifier);
        if (false !== $user && $user instanceof AbstractUserEntity && $user->isFullyCreated()) {
            return $user;
        }
        return null;
    }

    public function getSupportedUserClass(): string
    {
        return $this->supportedUserClass;
    }

    public function getUserGroups(): array
    {
        return $this->userGroups;
    }

    public function getUserGroupIdentifierByName(string $name): int
    {
        foreach ($this->userGroups as $user_group_identifier => $user_group_name) {
            if ($user_group_name === $name) {
                return $user_group_identifier;
            }
        }
        throw new Exception('User group "' . $name . '" doesn\'t exist.');
    }

    public function loadUserByUsername(string $username): ?UserInterface
    {
        $user_model = $this->makeUserModel();
        $conn = $this->getCmsDb()->getConnection();
        $query_builder = $conn->createQueryBuilder();
        $query_builder
            ->select('entity_id')
            ->from($user_model->getIdentityComponent()->getIndexTableName(), 'identity_index')
            ->where('identity_index.username = :username')
            ->setParameter('username', $username)
        ;
        $result = $query_builder->executeQuery();
        if (false === $row = $result->fetchAssociative()) {
            return null;
        }
        return $this->entityFactory->load($this->supportedUserClass, (int) $row['entity_id']);
    }

    public function getCmsDb(): CmsDb
    {
        return $this->cmsDb;
    }

    public function getEntityFactory(): EntityFactoryInterface
    {
        return $this->entityFactory;
    }

    public function makeUserModel(): AbstractUserEntity
    {
        $user_model = $this->entityFactory->make($this->supportedUserClass);
        if (false === $user_model instanceof AbstractUserEntity) {
            throw new Exception('Invalid class for the user entity user provider: "' . $this->supportedUserClass . '".');
        }
        return $user_model;
    }

}
