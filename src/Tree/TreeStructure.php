<?php

/*
 * Copyright 2023 TENTWELVE SRL
 * Licensed under the EUPL, Version 1.2 or - as soon they will be approved by
 * the European Commission - subsequent versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 * https://joinup.ec.europa.eu/software/page/eupl5
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

declare(strict_types=1);

namespace Vitya\CmsComponent\Tree;

use Exception;

class TreeStructure
{
    const MAX_RECURSION_LEVEL = 50;

    private $nodes = [];

    public function loadFromAssociativeMap(array $map, array $id_list = null): TreeStructure
    {
        $this->nodes = [];
        foreach ($map as $id => $parent_id) {
            if (false === is_int($id) || 0 >= $id) {
                continue;
            }
            if (false === is_int($parent_id) || 0 > $parent_id) {
                continue;
            }
            if (null === $id_list) {
                $this->addNode($id, $parent_id);
            } else {
                // Make sure that we don't insert into the tree structure an id that shouldn't be there.
                if (in_array($id, $id_list)) {
                    $this->addNode($id, $parent_id);
                }
            }
        }
        if (null !== $id_list) {
            // Make sure that every id in id list is present in the tree structure.
            foreach ($id_list as $id) {
                if (false === isset($this->nodes[$id])) {
                    $this->addNode($id, 0);
                }
            }
        }
        $reordered_nodes = $this->getChildNodes(0);
        $this->nodes = $reordered_nodes;
        return $this;
    }

    public function getChildNodes(int $id, int $recursion_level = 0): array
    {
        $child_nodes = array();
        if ($recursion_level > self::MAX_RECURSION_LEVEL) {
            return $child_nodes;
        }
        foreach ($this->nodes as $k => $parent_k) {
            if ($parent_k === $id) {
                $child_nodes[$k] = $parent_k;
                $child_nodes = $child_nodes + $this->getChildNodes($k, $recursion_level + 1);
            }
        }
        return $child_nodes;
    }

    public function process(): array
    {
        $raw_flat_list = [0 => []];
        $hierarchy = [0 => &$raw_flat_list[0]];
        $levels = [0 => 0];
        foreach ($this->nodes as $id => $parent_id) {
            $raw_flat_list[$id] = [];
            if (isset($raw_flat_list[$parent_id])) {
                $raw_flat_list[$parent_id][$id] = &$raw_flat_list[$id];
                $levels[$id] = $levels[$parent_id] + 1;
            } else {
                $raw_flat_list[0][$id] = &$raw_flat_list[$id];
                $levels[$id] = 1;
            }
        }
        return [
            'raw_flat_list' => $raw_flat_list,
            'hierarchy' => $hierarchy[0],
            'levels' => $levels,
        ];
    }

    public function getFlatList(): array
    {
        $process_result = $this->process();
        $flat_list = [];
        foreach ($process_result['raw_flat_list'] as $k => $v) {
            if (0 > $k) {
                $flat_list[] = $k;
            }
        }
        return $flat_list;
    }

    public function getHierarchy(): array
    {
        $process_result = $this->process();
        return $process_result['hierarchy'];
    }

    public function getLevels(): array
    {
        $process_result = $this->process();
        return $process_result['levels'];
    }

    public function getAssociativeMap(): array
    {
        return $this->nodes;
    }

    public function nodeIsPresent(int $id): bool
    {
        $id = $id;
        return (isset($this->nodes[$id]));
    }

    public function addNode(int $id, int $parent_id): TreeStructure
    {
        if (0 > $id || 0 > $parent_id) {
            throw new Exception('Invalid node id.');
        }
        if (false === isset($this->nodes[$id])) { // The new node must not be already present.
            if (isset($this->nodes[$parent_id])) { // The parent node must exist.
                $this->nodes[$id] = $parent_id;
            } else {
                $this->nodes[$id] = 0;
            }
        }
        return $this;
    }

    public function removeNode(int $id): TreeStructure
    {
        if (0 > $id) {
            throw new Exception('Invalid node id.');
        }
        if (isset($this->nodes[$id])) {
            unset($this->nodes[$id]);
        }
        foreach ($this->nodes as $node_id => $parent_id) {
            if ($parent_id === $id) { // No node can be a child of the removed node.
                $this->nodes[$node_id] = 0;
            }
        }
        return $this;
    }

    public function setParentId(int $id, int $parent_id): TreeStructure
    {
        if (0 > $id || 0 > $parent_id) {
            throw new Exception('Invalid node id.');
        }
        if (isset($this->nodes[$id]) && isset($this->nodes[$parent_id])) {
            if ($this->nodes[$id] !== $parent_id) {
                $this->removeNode($id);
                $this->addNode($id, $parent_id);
            }
        }
        return $this;
    }

}
