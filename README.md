# Vitya PHP content management system components

This package contains a collection of building blocks for a service
container-based CMS. It's based on the
[Vitya application framework](https://gitlab.com/vitya-system/application).

The easiest way to start is to tinstall
[the CMS application template](https://gitlab.com/vitya-system/cms-template).

Vitya aims to provide a simple CMS that can be easily extended. We try to keep
the code small and easy to understand.
