# Changelog

## [1.0.0-alpha12] - 2025-02-21
### Added
- Add image manipulation functionalities.

## [1.0.0-alpha12] - 2024-09-30
### Added
- Logger.

## [1.0.0-alpha11] - 2024-07-16
### Added
- Added Publication and PublicationEntityComponent.

## [1.0.0-alpha11] - 2024-07-17
### Added
- Many untracked additions.

## [1.0.0-alpha10] - 2022-07-01
### Added
- Added a Twig extension.
- Added a nonce service.
- Added a new datum type: LinkedEntitiesDatum.

### Changed
- Default ACLs have been modified.

## [1.0.0-alpha9] - 2022-06-03
### Changed
- Big refactoring.

## [1.0.0-alpha8] - 2021-08-02
### Changed
- Simplify data components handling.

## [1.0.0-alpha7] - 2021-07-05
### Added
- New l10n SDBCU component to store localized content.
### Changed
- Code refactoring.

## [1.0.0-alpha6] - 2021-06-07
### Changed
- Data components are now configurable.
- New DB structure.

## [1.0.0-alpha5] - 2021-05-03
### Added
- Data components now have datum subcomponents.
- SDBCU have a getTitle() method.
### Changed
- SDBCU component names can be changed. Now you can have one data1 and one data2
  components, for example.
- Data components are loaded and saved for real.
### Fixed
- Modification times are now correctly stored.

## [1.0.0-alpha4] - 2021-04-07
### Changed
- Bumped version to alpha4 to match the rest of the system.

## [1.0.0-alpha3] - 2021-03-01
### Added
- New CMS DB service.

## [1.0.0-alpha2] - 2021-02-01
### Added
- Add loading methods to the content unit factory.

## [1.0.0-alpha1] - 2021-01-04
### Added
- First release.
